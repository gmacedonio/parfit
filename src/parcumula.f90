subroutine parcumula
  ! Find coordinates of the centers of the gauss functions
  !
  use parmod
  use m_integ
  implicit none
  real(8) :: z,xdist,ydist,vinv
  integer :: nlvs
  integer :: i,j,k
  !
  nlvs = nzlev+1     ! Total number of levels
  do j=1,ntypes
     do k=0,nzlev
        dxdist(k)=windx(k)/vvj(k,j)
        dydist(k)=windy(k)/vvj(k,j)
        dvinv(k) =1d0/vvj(k,j)
     enddo
     do i=1,nsrc
        z = zsrc(i)
        call integ(nlvs,zeta,dz,dxdist,z,xdist)
        call integ(nlvs,zeta,dz,dydist,z,ydist)
        call integ(nlvs,zeta,dz,dvinv,z,vinv)
        rex(i,j) = xdist   ! Assume volcano is in the origin of coordinates
        rey(i,j) = ydist
        b(i,j) = 0.25d0/vinv
     enddo
  enddo
  return
end subroutine parcumula
