function fvcorr(v,z)
  ! Corrective factor for the settling velocity with altitude
  !
  !     z      -   Altitude in meters (input)
  !     v      -   Velocity at ground level (input)
  !     ivsm   -   Flag for the used model to calculate fvcorr
  !
  implicit none
  real(8) :: fvcorr,v,z
  integer :: ivsm
  real(8) bexp,a1,a2,a3
  !
  ivsm = 2                  ! Select model
  !
  if(ivsm == 0) then
     fvcorr = 1d0           ! no z-dependent
  elseif(ivsm == 1) then
     bexp = 0.000024d0
     fvcorr = exp(bexp*z)   ! z-dep. as in Macedonio et al 1988
  else
     if(v >= 14d0) then
        bexp = 0.0000745d0
        fvcorr = exp(bexp*z)
     elseif(v < 14d0.and.v >= 5d0) then
        bexp = 0.0000562d0+0.000012d0*v
        fvcorr = exp(bexp*z)
     elseif(v >= 2d0 .and. v < 5d0) then
        a1 = 12d0/v-6d0
        a2 = 1d0-0.6d0/v
        a3 = -200d0/v
        fvcorr =1d0+a1*1d-5*z+a2*1d-8*z**2+a3*1d-15*z**3
     elseif(v < 2d0 .and. v >= 1d0) then
        a1 = 1.5d0+6d0/v
        a2 = 2.5d0/v-1d0
        a3 = 245d0/v-180d0
        fvcorr =1d0+a1*1d-5*z+a2*1d-8*z**2+a3*1d-15*z**3
     elseif(v < 1d0 .and. v > 0.2d0 ) then
        a1 = 6.45d0-0.32d0/v
        a2 = -0.16d0-0.002d0/v
        a3 = 1.21d0/v+13.11d0
        fvcorr =1d0+a1*1d-5*z+a2*1d-8*z**2+a3*1d-15*z**3
     else
        fvcorr = 1d0
     endif
  endif
  return
end function fvcorr
