module settling
  ! This module provides the settling velocity models
  !
  ! Version: 1.5.1
  !
  ! Date: 22-SEP-2017
  !
  ! Provides:
  ! character function vset_model_name(number)
  ! integer   function vset_model_number(key)
  ! subroutine vsettl(diam,rhop,rhoa,visc,vset,model,psi,ierr)
  ! subroutine setpsi(psi,sphe,diam,modv,nc,ierr)
  ! subroutine get_gama(diam,sphe,gama,ierr)
  !
  use kindtype, only: ip,rp  ! Set the precision
  implicit none
  integer, parameter :: MAX_VSET_MODELS=6
  integer, parameter :: MAX_VSET_KEY_LENGTH=11
  integer, parameter :: MAX_VSET_NAME_LENGTH=24
  private :: strcompare  ! Compares two strings

  type :: vsmodel
     integer :: number                               ! Model number
     character(len=MAX_VSET_KEY_LENGTH)  :: key      ! Short name (key)
     character(len=MAX_VSET_NAME_LENGTH) :: name     ! Long name
  end type vsmodel

  ! Declare settling velocity models
  type(vsmodel), parameter :: settling_velocity_model(MAX_VSET_MODELS) = (/ &
       vsmodel (1,'arastopoour','Arastoopour et al., 1982'),  & ! Model 1
       vsmodel (2,'ganser',     'Ganser, 1993'),              & ! Model 2
       vsmodel (3,'wilson',     'Wilson & Huang 1979'),       & ! Model 3
       vsmodel (4,'dellino',    'Dellino et al., 2005'),      & ! Model 4
       vsmodel (5,'pfeiffer',   'Pfeiffer et al., 2005'),     & ! Model 5
       vsmodel (6,'dioguardi',  'Dioguardi et al. 2017')      & ! Model 6
       /)

contains

  character(len=MAX_VSET_NAME_LENGTH) function vset_model_name(number)
    ! Get vset long name from its number
    implicit none
    integer, intent(in) :: number
    if(number <= 0 .or. number > MAX_VSET_MODELS) then
       vset_model_name = 'Vset model unimplemented'
    else
       vset_model_name = settling_velocity_model(number)%name
    end if
  end function vset_model_name

  integer function vset_model_number(key) result(model)
    ! Get the model number from the model short name
    implicit none
    character(len=*), intent(in) :: key
    integer :: i
    model = 0
    do i=1,MAX_VSET_MODELS
       if(strcompare(key,settling_velocity_model(i)%key)) then
          model = i
          exit
       end if
    end do
  end function vset_model_number

  subroutine vsettl(diam,rhop,rhoa,visc,vset,model,psi,ierr)
    !
    !    Set fall velocity, as a function of particles diameter and
    !    air density and viscosity.
    !
    !     Models:
    !       1. Arastoopour et al. 1982
    !       2. Ganser 1993
    !       3. Wilson & Huang 1979
    !       4. Dellino et al. 2005
    !       5. Model of Pfeiffer et al., 2005
    !       6. Model of Dioguardi et al. 2017
    !
    !    NOTE1: The model of Pfeiffer et al., 2005 modifies the model of
    !           Wilson and Huang 1979 by introducing an interpolation between
    !           Rey=100 and Rey=1000, with Cd=1 for Rey>1000.
    !
    !    NOTE2: To account for Buoyancy force, set rhop = (rhop-rhoa) in the
    !           input parameters
    !
    !    diam     - Particle diameter in meters (Input)
    !    rhop     - Particle density (Input)
    !    rhoa     - Air density (Input)
    !    visc     - Air viscosity (Input)
    !    vset     - Particle settling velocity (Output)
    !    model    - Settling velocity model (Input)
    !    psi      - Form factor (Input)
    !    ierr     - Return status (0=OK, 1=Invalid model, 2=No convergence)
    !
    !***************************************************************************
    implicit none
    real(rp), intent(in)  :: diam,rhop,rhoa,visc,psi
    real(rp), intent(out) :: vset
    integer, intent(in)   :: model
    integer, intent(out)  :: ierr
    integer  :: it,maxit
    real(rp) :: gi,atol,rtol,rey,rk1,rk2,a,b,vold,cd100,cd1000
    real(rp) :: cd
    !
    ierr = 0                   ! Set initial value for return status
    gi   = 9.81_rp             ! Gravity acceleration
    atol = 1e-12_rp            ! Absolute tolerance
    rtol = 1e-6_rp             ! Relative tolerance
    maxit= 1000                ! Maximum number of iterations
    cd   = 1.0_rp              ! Guess value
    !
    ! Check model number
    if(model <=0 .or. model > MAX_VSET_MODELS) then
       ierr = 1
       return
    end if

    select case (settling_velocity_model(model)%number)
    case (1)
       ! 1. Model of Arastoopour et al. 1982
       vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
       vold=vset
       do it=1,maxit
          rey=rhoa*vset*diam/visc
          if(rey <= 988.947_rp) then   ! This is the actual transition point
             cd=24.0_rp/rey*(1.0_rp+0.15_rp*rey**0.687_rp)
          else
             cd=0.44_rp
          endif
          vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
          if(abs(vset-vold) <= atol+rtol*abs(vset)) return
          vold=vset
       end do
       !
    case(2)
       ! 2. Model of Ganser 1993
       vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
       vold=vset
       do it=1,maxit
          rey=rhoa*vset*diam/visc
          rk1=3.0_rp/(1.0_rp+2.0_rp/sqrt(psi))  ! Stokes' shape factor
          rk2=10.0_rp**(1.8148_rp*(-log10(psi))**0.5743_rp) !Newton's shape fact
          cd=24.0_rp/(rey*rk1)*(1.0_rp+0.1118_rp*(rey*rk1*rk2)**0.6567_rp) + &
               0.4305_rp*rk2/(1.0_rp+3305.0_rp/(rey*rk1*rk2))
          vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
          if(abs(vset-vold) <= atol+rtol*abs(vset)) return
          vold=vset
       end do
       !
    case(3)
       ! 3. Model of Wilson & Huang 1979
       vset=sqrt(4d0*gi*diam*rhop/(3d0*cd*rhoa))
       vold=vset
       do it=1,maxit
          rey=rhoa*vset*diam/visc
          cd=24d0/rey*psi**(-0.828d0)+2d0*sqrt(1.07d0-psi)
          vset=sqrt(4d0*gi*diam*rhop/(3d0*cd*rhoa))
          if(abs(vset-vold) <= atol+rtol*abs(vset)) return
          vold=vset
       enddo
       !
    case(4)
       ! 4. Model of Dellino et al. 2005
       vset = ((diam**3*gi*rhop*rhoa*(psi**1.6_rp))/(visc**2))**0.5206_rp
       vset = (1.2065_rp*visc*vset)/(diam*rhoa)
       return
       !
    case(5)
       ! 5. Model of Pfeiffer et al., 2005
       vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
       vold=vset
       cd100=0.24_rp*psi**(-0.828_rp)+2.0_rp*sqrt(1.07_rp-psi) ! cd at rey=100
       cd1000=1.0_rp            ! cd at rey>=1000 (from Pfeiffer et al., 2005)
       !cd1000=1d0-0.56d0*psi
       a=(cd1000-cd100)/900.0_rp                            ! interpolation
       b=cd1000-1000.0_rp*a
       do it=1,maxit
          rey=rhoa*vset*diam/visc
          if(rey <= 100.0_rp) then
             cd=24.0_rp/rey*psi**(-0.828_rp)+2.0_rp*sqrt(1.07_rp-psi)
          elseif(rey > 100.0_rp .and. rey < 1000.0_rp) then
             cd=a*rey+b
          else
             cd=cd1000
          endif
          vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
          if(abs(vset-vold) <= atol+rtol*abs(vset)) return
          vold=vset
       end do
    case(6)
       ! 6. Model of Dioguardi et al. 2017
       vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
       vold=vset
       do it=1,maxit
          rey=rhoa*vset*diam/visc
          ! Evaluate cd_sphere
          cd = 24.0_rp*(1.0_rp+0.15_rp*rey**0.687_rp)/rey + &
               0.42_rp/(1.0_rp+42500.0_rp/rey**1.16_rp)
          ! Note: the Reynolds number is limited to 10^-2 in the exponent
          ! to avoid the strong divergence near Re=0. This limit is compatible
          ! with the range of Reynolds numbers investigated in the experiments
          ! of Dioguardi et al., 2017.  psi is the sphericity
          cd = 0.74533_rp*rey**0.146012_rp*cd / &
               psi**(0.5134_rp/max(rey,0.01_rp)**0.2_rp)
          vset=sqrt(4.0_rp*gi*diam*rhop/(3.0_rp*cd*rhoa))
          if(abs(vset-vold) <= atol+rtol*abs(vset)) return
          vold=vset
       end do
       !
    case default
       ! *** Model not set
       !
       ierr = 1
       return
    end select
    !
    !*** No convergence
    !
    ierr = 2
    return
    !
  end subroutine vsettl

  subroutine setpsi(psi,sphe,diam,modv,nc,ierr)
    !*********************************************************************
    !*
    !*    Calculates the particle shape factor psi from the sphericity,
    !*    depending on the velocity model
    !*
    !*    modv = 1   ARASTOPOUR. psi = 1 (not used)
    !*    modv = 2   GANSER      psi = sphericity
    !*    modv = 3   WILSON      psi = (b+c)/2a    a>b>c semi-axes
    !*    modv = 4   DELLINO     psi = sphericity/circularity
    !*    modv = 5   DIOGUARDI   psi = sphericity
    !*
    !*    ierr - Return status (0=OK, 1=Invalid model, 2=No convergence)
    !**********************************************************************
    implicit none
    integer(ip) :: modv,nc
    real(rp), intent(in)  :: sphe(nc),diam(nc)
    real(rp), intent(out) :: psi(nc)
    integer(ip), intent(out) :: ierr
    !
    integer(ip) :: ic
    real   (rp) :: pi,gama,circula
    !
    !***    Initializations
    !
    pi = 4.0_rp*atan(1.0_rp)
    ierr = 0
    !
    !***    Computes psi
    !
    if(modv == 1) then          ! Arastopour
       !
       psi(1:nc) = 1.0_rp
       !
    else if(modv == 2) then     ! Ganser
       !
       psi(1:nc) = sphe(1:nc)
       !
    else if(modv == 3) then     ! Wilson
       !
       do ic = 1,nc
          call get_gama(diam(ic),sphe(ic),gama,ierr)  ! Get a/c
          if(ierr /= 0) return
          if(gama >= 1.0_rp) then                ! oblate
             psi(ic) = 0.5_rp*(1.0_rp+1.0_rp/gama)
          else                                ! prolate
             psi(ic) = gama
          end if
       end do
       !
    else if(modv == 4)  then    ! Dellino
       !
       do ic = 1,nc
          call get_gama(diam(ic),sphe(ic),gama,ierr)         ! Get a/c
          if(ierr /= 0) return
          if(gama >= 1.0_rp) then                       ! oblate
             circula = 1.0_rp
             psi(ic) = sphe(ic)/circula
          else                                    ! prolate
             circula = sqrt(gama)                 ! Riley 1941
             psi(ic) = sphe(ic)/circula
          end if
       end do
       !
    else if(modv == 5) then     ! Dioguardi 2017
       !
       psi(1:nc) = sphe(1:nc)
       !
    else
       ierr = 1
    end if
    !
    return
  end subroutine setpsi

  subroutine get_gama(diam,sphe,gama,ierr)
    !**************************************************************************
    !*
    !*     Gets gama = a/c
    !*
    !*     Prolate spheroid: a=minor semi-axis, c=major semi-axis
    !*
    !*
    !*     NOTE: In all cases it is assumed that particles are prolate ellipsoids
    !*
    !*     a = b < c  prolate   (gama < 1)
    !*
    !*     The inversion of the area of the ellipsoid is done numerically.
    !*     Area given by:
    !*
    !*     A = 2*pi*(a**2 + c**2*e/tan(e) )   e = acos(gama)
    !*     d = 2*c*gama**(2/3)               (prolate)
    !*
    !*     NOTE: particle diameter is multiplied by a factor. It does not affect
    !*           results (a/c) and is done to facilitate convergence and prevent
    !*           propagation of rounding errors (e.g. for micron size particles
    !*           diam of the order 1d-6 rised to 2 or 3)
    !*
    !*     ierr - Return status (0=OK, 1=No convergence)
    !***************************************************************************
    implicit none
    real(rp), intent(in)     :: diam,sphe
    real(rp), intent(out)    :: gama
    integer(ip), intent(out) :: ierr
    !
    real(rp), parameter :: pi=3.14159265358979_rp   ! Greek PI
    integer(ip) :: iiter,niter
    real   (rp) :: d,gmin,gmax,Ao,toler,e
    real   (rp) :: Ap
    !
    !***   Initializations
    !
    ierr = 0
    d     = diam*1e3_rp         ! see NOTE
    niter = 1000
    toler = 1e-8_rp
    gmin  = 1e-3_rp
    gmax  = 1.0_rp
    !
    !***  Surface area
    !
    Ap = 4.0_rp*pi*(0.5_rp*d)**2 ! d=diameter of the sphere with same volume
    !
    !***   Iterates
    !
    do iiter = 1,niter
       gama = 0.5_rp*(gmin+gmax)
       e    = acos(gama)
       Ao   = 0.5_rp*pi*d*d*(gama**(-4.0_rp/3.0_rp))*(gama*gama + (e/tan(e)))
       if(Ao < Ap) then
          gmax = gama
       else
          gmin = gama
       end if
       if((iiter > 1).and.(abs(Ao-Ap) < toler)) return  ! Convergence
    end do
    !
    !*** No convergence
    ierr = 1

  end subroutine get_gama

  logical function strcompare(str1,str2)
    ! Compare two strings (no case sensitive)
    implicit none
    character(len=*), intent(in)  :: str1,str2
    integer :: i,ia1,ia2,nstr1,nstr2
    integer, parameter :: ua=iachar('A'), uz=iachar('Z')
    integer, parameter :: delta=iachar('a')-iachar('A')
    nstr1 = len_trim(str1)
    nstr2 = len_trim(str2)
    strcompare = .true.
    if(nstr1 /= nstr2) then
       strcompare = .false.
    else
       do i=1,nstr1
          ia1 = iachar(str1(i:i))
          ia2 = iachar(str2(i:i))
          if(ia1 >= ua .and. ia1 <= uz) ia1 = ia1 + delta ! -> lowercase
          if(ia2 >= ua .and. ia2 <= uz) ia2 = ia2 + delta ! -> lowercase
          if(ia1 /= ia2) then
             strcompare = .false.
             exit
          end if
       end do
    end if
  end function strcompare

end module settling
