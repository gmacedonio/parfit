!> Module or managing the input parameters of parfit
module m_parfit_input
  ! Author: Giovanni Macedonio
  !
  ! Date of this version: 20-DEC-2024
  !
  use m_inpout
  use m_inpoutext
  use m_angles
  implicit none

  type :: parfit_input
     real(8) :: htropo                 ! Height of tropopause
     real(8) :: wndmin,wndmax,wndstep  ! Minimum, maximum, step for wind int.
     real(8) :: dirmin,dirmax,dirstep  ! Minimum, maximum, step for wind direction
     real(8) :: cdmin,cdmax,cdstep  ! Diffusion coefficient: min,max,step
     real(8) :: hcolmin,hcolmax,hcolstep
     real(8) :: suz1min,suz1max,suz1step
     real(8) :: suz2min,suz2max,suz2step
     real(8) :: hlayer              ! Layer thickness
     real(8) :: xvent,yvent,zvent
     real(8) :: belthull            ! Thickness of the belt around convex hull
     integer :: modew               ! Chi2 weighting mode
     logical :: read_wind_file      ! Flag for reading the wind file
     logical :: write_chi2          ! Write chi2 file
     logical :: check_convex_hull   ! Flag: use convex hull
     logical :: enlarge_convex_hull ! Flag: enlarge the convex hull
     logical :: use_nearest_points  ! Use nearest points for spectra
     integer :: vmodel              ! Settling velocity model
     integer :: vset_of_z           ! Settling velocity is a function of Z
     logical :: scalepar = .true.   ! Scale parameters (for bobyqa and direct)
     character(len=20)  :: tgsd_method  ! Use INPUT or SEARCH from ground gsd
     character(len=20)  :: suzmodel     ! Suzuki model
     character(len=255) :: ground_thickness = 'ground_thickness.inp'  ! Default
     character(len=255) :: ground_gsd_file  = 'ground_grain_size.inp' ! Default
     character(len=255) :: tgsd_file  = 'total_grai_size.inp'         ! Default
     character(len=255) :: input_wind_file  = 'wind.dat'              ! Default
     character(len=255) :: chi2_file
     ! Optimization methods
     character(len=20)  :: optim_method  ! Optimization method
     real(8) :: initial_diameter     ! Initial diameter used by BOBYQA/VTDIRECT
     real(8) :: minimum_diameter     ! Minimum diameter used by BOBYQA/VTDIRECT
     integer :: maxfun               ! Max number of function calls
     character(len=3) :: utm_zone    ! UTM zone (reserved for future versions)
   contains
     procedure :: load => parfit_input_load
  end type parfit_input

contains

  !> Load input parameters
  subroutine parfit_input_load(params, fconf)
    class(parfit_input) :: params
    character(len=*), intent(in) :: fconf
    integer :: status
    character(len=20) :: answer   ! Temporary answer

    ! Load the configuration file (load in the default hashtable)
    call read_and_store(fconf, status)
    if(status /= 0) call abort_program('Cannot load file:',fconf)

    ! Coordinate system
    call mandatory('UTM_ZONE', params%utm_zone)

    ! Input wind file
    params%read_wind_file = .false.
    call optional('READ_WIND_FILE', answer, 'no')
    if(answer .equal. 'yes') then
       params%read_wind_file = .true.
    else
       ! Ranges of the search parameters
       call mandatory('WIND_SPEED_MIN',  params%wndmin)
       call mandatory('WIND_SPEED_MAX',  params%wndmax)
       call mandatory('WIND_SPEED_STEP', params%wndstep)

       ! Angles are given in degrees and internally transformed in radians
       call mandatory('WIND_DIR_MIN',  params%dirmin)
       call mandatory('WIND_DIR_MAX',  params%dirmax)
       call mandatory('WIND_DIR_STEP', params%dirstep)
       params%dirmin = rad(params%dirmin)       ! Convert to radiants
       params%dirmax = rad(params%dirmax)       ! Convert to radiants
       params%dirstep= rad(params%dirstep)      ! Convert to radiants
    end if

    ! Files
    call mandatory('TOTAL_GRAIN_SIZE_METHOD', params%tgsd_method)
    if(.not.(params%tgsd_method .equal. 'input') .and. &
         .not.(params%tgsd_method .equal. 'search')) then
       call abort_program('Invalid parameter TOTAL_GRAIN_SIZE_METHOD:',params%tgsd_method)
    end if

    call optional('TOTAL_GRAIN_SIZE_FILE', params%tgsd_file, 'total_grain_size.inp')
    call optional('GROUND_GRAIN_SIZE_FILE', params%ground_gsd_file, 'ground_grain_size.inp')
    call optional('GROUND_THICKNESS_FILE', params%ground_thickness, 'ground_thickness.inp')
    call optional('INPUT_WIND_FILE', params%input_wind_file, 'wind.dat')

    ! Optimization method (direct, bobyqa, loops)
    call optional('OPTIMIZATION_METHOD', params%optim_method, 'loops')
    call convert_to_uppercase(params%optim_method)
    params%scalepar = .true.
    call optional('SCALE_PARAMETERS', answer, 'yes')
    if(answer .equal. 'no') params%read_wind_file = .false.
    ! Search diameters used by vtdirect and bobyqa
    call optional('INITIAL_SEARCH_DIAMETER', params%initial_diameter, 1d-1)
    call optional('MINIMUM_SEARCH_DIAMETER', params%minimum_diameter, 1d-4)
    call optional('MAXIMUM_FUNCTION_CALLS',  params%maxfun, 2000)

    call mandatory('DIFFUSION_COEFFICIENT_MIN',  params%cdmin)
    call mandatory('DIFFUSION_COEFFICIENT_MAX',  params%cdmax)
    call mandatory('DIFFUSION_COEFFICIENT_STEP', params%cdstep)

    call mandatory('COLUMN_HEIGHT_MIN',  params%hcolmin)
    call mandatory('COLUMN_HEIGHT_MAX',  params%hcolmax)
    call mandatory('COLUMN_HEIGHT_STEP', params%hcolstep)

    call mandatory('SUZUKI_PARAMETER_A_MIN',  params%suz1min)
    call mandatory('SUZUKI_PARAMETER_A_MAX',  params%suz1max)
    call mandatory('SUZUKI_PARAMETER_A_STEP', params%suz1step)

    call mandatory('SUZUKI_PARAMETER_L_MIN',  params%suz2min)
    call mandatory('SUZUKI_PARAMETER_L_MAX',  params%suz2max)
    call mandatory('SUZUKI_PARAMETER_L_STEP', params%suz2step)

    ! Chi2 weighting mode
    call mandatory('CHI2_WEIGHTING_MODE', params%modew)

    ! Suzuki model
    call optional('SUZUKI_MODEL', params%suzmodel, 'integrated')

    ! Atmosphere
    call mandatory('ATMOSPHERIC_LAYER_THICKNESS', params%hlayer)
    call mandatory('TROPOPAUSE_HEIGHT', params%htropo)

    ! Settling velocity model
    call mandatory('SETTLING_VELOCITY_MODEL', params%vmodel)
    call mandatory('SETTLING_FUNCTION_OF_Z', params%vset_of_z)

    ! Vent coordinates
    call mandatory('VENT_COORDINATE_UTM_X', params%xvent)
    call mandatory('VENT_COORDINATE_UTM_Y', params%yvent)
    call mandatory('VENT_COORDINATE_Z', params%zvent)

    ! Convex hull
    call optional('CHECK_CONVEX_HULL', answer, 'no')
    if(answer .equal. 'yes') params%check_convex_hull = .true.

    if(params%check_convex_hull) then
       call optional('ENLARGE_CONVEX_HULL', answer, 'no')
       if(answer .equal. 'yes') params%enlarge_convex_hull = .true.
       if(params%enlarge_convex_hull) then
          call mandatory('CONVEX_HULL_BELT_THICKNESS', params%belthull)
       end if
    end if

    call optional('USE_NEAREST_POINTS', answer, 'no')
    if(answer .equal. 'yes') params%use_nearest_points = .true.

  end subroutine parfit_input_load

end module m_parfit_input
