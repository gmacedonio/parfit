subroutine chi2(rchi2,npts,out,ground,weight)
  ! Compute chi2
  implicit none
  !
  integer :: npts
  real(8) :: rchi2
  real(8) :: out(npts),ground(npts),weight(npts)
  real(8) :: dchi2
  integer :: i
  !
  dchi2 = 0d0
  do i=1,npts
     dchi2 = dchi2 + dble(((out(i)-ground(i))**2)*weight(i))
  enddo
  rchi2 = dchi2
  return
end subroutine chi2
