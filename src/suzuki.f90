subroutine suzuki (suz,nsrc,asuz1,asuz2)
  !     suzuki - Calculation of mass distribution along the column,
  !     using the Suzuki formula.
  !
  use m_safeop
  implicit none
  integer  :: nsrc
  real(8) :: suz(nsrc)
  real(8) :: asuz1,asuz2
  real(8) :: x,dh
  real(8) :: sum
  integer  :: i
  !
  dh=1d0/nsrc   ! Normalized vertical step
  x=1d0         ! Normalized distance from the column top
  sum=0d0
  do i=1,nsrc
     x = max(x-dh,0d0)    ! Avoid negative values due to round-offs
     suz(i)=(x*exp(-asuz1*x))**asuz2
     sum=sum+suz(i)
  enddo
  !
  !.... Renormalize
  do i=1,nsrc
     suz(i)=safediv(suz(i),sum)
  enddo
end subroutine suzuki
