#
.PHONY: all biblio clean veryclean export

# LaTeX and BiBTeX files
MANUAL=parfit-manual
BIBFILE=parfit.bib

# Main biblio database (used to build the BIBFILE file)
MAINBIB=$(HOME)/tex/bibtex/references.bib

all: $(MANUAL).pdf

biblio: $(BIBFILE)

%.pdf: %.tex $(BIBFILE)
	pdflatex $*
	bibtex $*
	@latex_count=8; \
	while egrep -s 'Rerun to get' $*.log && [ $$latex_count -gt 0 ] ; do \
	  echo "Rerunning latex...." ;\
	  pdflatex $* ;\
	  latex_count=`expr $$latex_count - 1` ; \
	done

$(BIBFILE): $(MANUAL).tex
	pdflatex $(MANUAL)
	@if [ -r $(MAINBIB) ]; then \
	  bibtool -q -x $(MANUAL).aux -i $(MAINBIB) -o $(BIBFILE); \
	fi
	pdflatex $(MANUAL)

clean:
	rm -f *~ *.aux *.log *.lot *.lof *.toc *.blg *.out *.idx

veryclean:
	make -s clean
	rm -f *.bbl
	rm -f $(MANUAL).pdf
