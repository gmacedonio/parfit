program parfit
  !
  ! Find best fit parameters between hazmap model and deposit data
  !
  ! Authors: Giovanni Macedonio, Antonio Costa
  !
  ! Date of this version: 26-JUL-2024
  !
  ! Note: parfit was firstly derived from hazmap version 2.3.1
  ! This version is compatible with hazmap 2.4
  !
  use config
  use parmod
  use airprop
  use settling
  use m_safeop
  use m_angles
  use m_errors
  use m_bobyqa
  use m_vtdirect
  use m_windutils
  use m_parfit_input
  implicit none
  character(len=80) :: message
  !
  integer :: npts                       ! Number of grid point
  !
  ! Command line arguments
  integer :: narg                ! Number of arguments in the command line
  integer :: lenpref
  !
  ! Parameters for fit
  integer, parameter :: npar = 6
  type(parfit_input) :: params        ! Parfit input parameters
  integer :: npt
  real(8) :: rhobeg
  integer :: max_iter                 ! Used by vtdirect
  real(8) :: funmin                   ! Used by vtdirect
  logical :: scalepar = .true.        ! Flag internal scaling of parameters
  real(8), allocatable :: par(:)      ! Model parameters
  real(8), allocatable :: parm(:)     ! Model parameters at min chi2
  integer :: iprint = 0               ! 0=does not print (BOBYQA)
  integer :: status
  !
  !.... File names
  character(len=255) :: problem  ! Files problem
  character(len=255) :: finp    ! Input file  (data)
  character(len=255) :: fout    ! Output file (ascii)
  character(len=255) :: fwnd    ! Output wind file
  character(len=255) :: fcmp    ! Output comparison file
  character(len=255) :: fchi    ! Output file for chi2 info
  !
  ! Unit numbers
  integer :: ngrd              ! Input ground thicknesses
  integer :: ncsp              ! Input total size distribution
  integer :: ngsp              ! Input ground size distribution
  integer :: ncmp              ! Output comparison file
  integer :: nchi              ! Output chi2

  real(8) :: xgtmp,ygtmp

  real(8) :: totmm          ! Initial guess mass (for initialization)
  real(8) :: rhodre         ! Density Rock Equivalent
  integer  :: ntropo        ! Index of Tropopause layer
  integer, allocatable :: iwork1(:),iwork2(:) ! Work vector for subr. convex
  integer, allocatable :: indss(:)       ! Work vector for subroutine convex
  integer, allocatable :: in(:)          ! Work vector for subroutine convex
  integer, allocatable :: il(:)          ! Work vector for subroutine convex
  integer, allocatable :: ih(:)          ! Work vector for subroutine convex
  real(8), allocatable :: xhull(:),yhull(:)   ! Vertices of the convex hull
  real(8), allocatable :: wground(:)
  real(8), allocatable :: out(:,:)            ! Deposit loadings
  real(8), allocatable :: xg(:,:)             ! Ground section coordinate

  integer :: iwndm                  ! Index of best hazwind
  integer :: nwnd,ndir              ! Number of wind intensities and direction
  real(8), allocatable :: rmoss(:)  ! Observed deposit (temporary variable)
  real(8), allocatable :: rmmod(:)  ! Modelled deposit (temporary variable)
  real(8), allocatable :: wvel(:)   ! Wind profile
  real(8), allocatable :: dthick(:) ! Deposit thicknesses
  real(8), allocatable :: ddens(:)  ! Deposit densities
  real(8), allocatable :: ground(:) ! Deposit loadings
  real(8), allocatable :: pdiam(:)  ! Particle diameter
  real(8), allocatable :: pdens(:)  ! Particle density
  real(8), allocatable :: ppsi(:)   ! Particle shape factor
  real(8), allocatable :: low(:)    ! Lower bounds of the parameters
  real(8), allocatable :: up(:)     ! Upper bounds of the parameters

  real(8), allocatable :: fsj(:,:)  ! Fraction of particles in the sections
  character(len=6), allocatable :: labgrd(:)    ! Label of cell with deposit
  character(len=6), allocatable :: labhul(:)    ! Label vertices of convex hull
  character(len=6), allocatable :: labgsp(:)    ! Label of cell with GSD
  character(len=256) :: tstring     ! Temporary string
  real(8) :: fvcorr                 ! Function for correction of vset(z)

  integer :: ncd
  integer :: nhcol
  real(8), allocatable :: fp(:)   ! Particle TGSD in the column (fraction)
  integer :: nsuz1,nsuz2
  integer :: i,j,k,ik,ind
  integer :: nptsp    ! Number of ground sections with GSD
  real(8) :: dzw
  logical, allocatable :: include_class(:)  ! Include particle type
  real(8), allocatable :: totout(:)
  real(8), allocatable :: weight(:)
  integer :: icd, idir, ihcol, isuz1, isuz2
  integer :: ifound
  integer :: ntotsp        ! Number of sections with GSD
  integer :: kmin
  integer :: indmin
  real(8) :: dist,dismin   ! Temporary variables
  integer :: nhull
  real(8) :: dir
  real(8) :: hcol
  integer :: iwnd
  real(8) :: wnd
  real(8) :: suz1,suz2     ! Suzuki coefficients
  integer :: ntest
  real(8) :: rmost
  integer :: ins
  real(8) :: fmass
  real(8) :: fmout
  real(8) :: xmass,ymass
  real(8) :: beta
  real(8) :: cgrd
  real(8) :: rchi2, rchi2m
  real(8) :: sumvs
  real(8) :: cout
  real(8) :: fmin
  real(8) :: rhoa,pres,temp     ! Atmosphere density, pressure and temperature
  real(8) :: sigma,delta,theta  ! Air dens,pres,temp respect reference
  real(8) :: visca              ! Air viscosity
  integer :: ierr
  integer :: nctypes            ! Number of considered particles types
  integer :: nsrcmax            ! Maximum number of point sources
  real(8) :: srcstep            ! Vertical step between sources
  !
  type(hazwinds) :: winds       ! Winds in hazmap format
  !
  ! Get command line argument
  narg = command_argument_count()
  if(narg /= 1) then
     write(*,'(''parfit '',a,1x,''('',a,'')'')') version, git_version
     write(*,'(''Usage: parfit problem[.inp]'')')
     call exit(1)
  end if
  call get_command_argument(1,problem)

  ! Generate file names
  lenpref = len_trim(problem)
  if(lenpref > 4) then
     if(problem(lenpref-3:lenpref) == '.inp') problem = problem(1:lenpref-4)
  end if
  finp = trim(problem)//'.inp'
  fout = trim(problem)//'.out'
  fcmp = trim(problem)//'.cmp'
  fwnd = trim(problem)//'.wnd'
  fchi = trim(problem)//'.chi2'
  !
  ! Set guess mass
  totmm  = 1e10               ! This is arbitrary
  rhodre = 2600.              ! Rock density (for DRE)
  !
  ! Read input file
  !
  call params%load(finp)
  !
  ! Open output file
  !
  call get_free_unit(nout)
  if(nout < 0) call abort_program('Cannot get file unit (nout)')
  open(nout,file=fout,status='unknown',err=2004)
  !
  ! Write version number
  write(message,'(''Parfit version: '',a)') version
  call write_out(message)

  nwnd   = nint(safediv(params%wndmax-params%wndmin,params%wndstep))
  ndir   = nint(safediv((params%dirmax-params%dirmin),params%dirstep))
  ncd    = nint(safediv((params%cdmax-params%cdmin),params%cdstep))
  nhcol  = nint(safediv(params%hcolmax-params%hcolmin,params%hcolstep))
  ! Next line was commented starting from version 2.2.1
  ! hstep  = hcolstep  ! <= OLD, keep commented: set vertical step = hcolstep
  ! Variable hstep becomes hlayer
  nsuz1  = nint(safediv(params%suz1max - params%suz1min, params%suz1step))
  nsuz2  = nint(safediv(params%suz2max - params%suz2min, params%suz2step))
  !
  ! Wind and z-layers
  !
  if(.not.params%read_wind_file) then   ! WIND is generated internally
     ! Estimate the number of layers up to tropopause
     ntropo = nint(safediv(params%htropo, params%hlayer))
     ! Generate zeta (wind layers) levels
     !
     dzw = params%htropo/ntropo          ! Thickness of wind layers
     nzlev = ceiling(params%hcolmax/dzw) ! Max. number of wind layers (may be < ntropo)
     ntropo = min(ntropo,nzlev)   ! Don't need wind layers above nzlev
     ! Set wind layers
     allocate(zeta(0:nzlev))
     do i = 0,nzlev
        zeta(i) = i*dzw
     enddo
     ! Allocate memory for wind profile
     allocate(wvel(0:nzlev))
     ! Generate normalized wind profile
     call genprotowind(nzlev, zeta, params%htropo, ntropo, wvel)
     ! windx and windy are set in the loop on parameters
     allocate(windx(0:nzlev))
     allocate(windy(0:nzlev))
     !
  else  ! WIND is read from file
     !
     ! Read wind file
     write(message,'(''Using wind file: '',a)') trim(params%input_wind_file)
     call write_out(message)
     !
     call hazwinds_read(winds, params%input_wind_file)
     write(message,'(''Number of read wind profiles: '',i8)') winds%nwinds
     call write_out(message)
     ndir = 0   ! Does not loop on directions
     nzlev = winds%nz
     nwnd = winds%nwinds
     allocate(windx(0:nzlev))
     allocate(windy(0:nzlev))
     allocate(zeta(0:nzlev))
     zeta(0:nzlev) = winds%zeta(0:winds%nz)
  end if
  !
  ! Evaluate DZ (used by parcumula)
  allocate(dz(0:nzlev))
  do i = 1,nzlev
     dz(i) = zeta(i)-zeta(i-1)
  enddo
  dz(0) = dz(1)
  !
  if(params%write_chi2) then
     call get_free_unit(nchi)
     if(nchi < 0) call abort_program('Cannot get file unit (nchi)')
     open(nchi,file=fchi,status='unknown')
  endif
  !
  !
  if(params%tgsd_method .equal. 'INPUT') then  ! Read TGSD from file
     call get_free_unit(ncsp)
     if(ncsp < 0) call abort_program('Cannot get file unit (ncsp)')
     open(ncsp, file=params%tgsd_file, status='old', err=2006)
     ! Read number of particle types
     read(ncsp,*) ntypes
     !
     allocate(pdiam(ntypes))
     allocate(pdens(ntypes))
     allocate(ppsi(ntypes))
     allocate(fp(ntypes))
     !
     ! Read diameter, density, shape, percentage
     do j=1,ntypes
        read(ncsp,*) pdiam(j),pdens(j),ppsi(j),fp(j)
     enddo
     ! Convert to fractions
     fp = fp/100.
     !
     close(ncsp)
     write(message,'(''Read total grain size distribution from file: '',a)') trim(params%tgsd_file)
     call write_out(message)
  endif
  !
  ! Estimate maximum number of source points (for memory allocation)
  nsrcmax = ceiling(params%hcolmax/params%hlayer)
  !
  !
  ! Allocate memory
  allocate(dxdist(0:nzlev))
  allocate(dydist(0:nzlev))
  allocate(dvinv(0:nzlev))
  !
  ! Open/Read ground thickness and deposit density
  !
  call get_free_unit(ngrd)
  if(ngrd < 0) call abort_program('Cannot get file unit (ngrd)')
  open(ngrd, file=params%ground_thickness, status='old', err=2003)
  ! Count the number of points (lines)
  npts=0
  do
     read(ngrd,'(a)',end=100,err=2008) tstring
     if(tstring(1:1) == '#') cycle   ! Skip comments
     npts = npts + 1
  end do
100 continue
  rewind(ngrd)
  !
  ! Allocate memory
  allocate(labgrd(npts))
  allocate(xg(2,npts))
  allocate(dthick(npts))
  allocate(ddens(npts))
  allocate(ground(npts))
  allocate(totout(npts))
  allocate(weight(npts))
  allocate(wground(npts))
  allocate(in(npts))
  allocate(ih(npts))
  allocate(il(npts))
  allocate(xhull(npts))
  allocate(yhull(npts))
  allocate(labhul(npts))
  allocate(zsrc(nsrcmax))
  allocate(iwork1(npts))
  allocate(iwork2(npts))
  iwork1=0       ! Clear vector
  iwork2=0       ! Clear vector
  !
  i=1
  tstring = ''   ! Clear
  do
     read(ngrd,'(a)',end=101) tstring
     if(tstring(1:1) == '#') cycle  ! Skip comments
     read(tstring,*,end=2008,err=2008) labgrd(i),xgtmp,ygtmp,dthick(i),ddens(i)
     xg(1,i) = (xgtmp - params%xvent)
     xg(2,i) = (ygtmp - params%yvent)
     ground(i) = dthick(i)*ddens(i)
     in(i)=i             ! Needed by subroutine convex
     i=i+1
  enddo
101 continue
  if(i-1 /= npts) call abort_program('Problems reading ground thickness file')
  !
  close(ngrd)
  !
  !.... Check whether two sections have the same number
  do i=1,npts-1
     do j=i+1,npts
        if(labgrd(i) == labgrd(j)) then
           write(message,'(''ERROR: Two or more sections have the &
                &same number: '',a,'' in file '',a)') trim(labgrd(i)), &
                trim(params%ground_thickness)
           call abort_program(message)
        endif
     enddo
  enddo
  !
  write(message,'(''Number of ground sections: '',i4)') npts
  call write_out(message)
  !
  ! Generate weights for chi2 (deposit)
  call depw(npts, ground, weight, params%modew)
  !
  ! Open/Read ground particles GSD
  !
  if(params%tgsd_method .equal. 'SEARCH') then
     write(message,'(''Using ground grain size distribution from file: '',a)') trim(params%ground_gsd_file)
     call write_out(message)
     call get_free_unit(ngsp)
     if(ngsp < 0) call abort_program('Cannot get file unit (ngsp)')
     open(ngsp, file=params%ground_gsd_file, status='old', err=2005)
     !
     ! Read number of particle types
     read(ngsp,*) ntypes
     !
     allocate(pdiam(ntypes))
     allocate(pdens(ntypes))
     allocate(ppsi(ntypes))
     allocate(fp(ntypes))
     !
     ! Read particles diameter, density and shape
     do j=1,ntypes
        read(ngsp,*) pdiam(j),pdens(j),ppsi(j)
     enddo
     !
     ! Read GSD in the ground sections
     read(ngsp,*) nptsp
     !
     if(nptsp > npts) then
        write(message,'(''Number of ground sections with GSD is &
             &greater than NPTS='',i5)') npts
        call abort_program('Check input files')
     endif
     !
     allocate(labgsp(nptsp))        ! Allocate only for nptsp points
     allocate(fsj(npts,ntypes))     ! Allocate for all npts points
     allocate(indss(npts))          ! Allocate for all npts points
     indss = 0                      ! Clear indss
     !
     ! Read percentage of each particle
     do i=1,nptsp
        read(ngsp,*) labgsp(i)
        do j=1,ntypes
           read(ngsp,*) fsj(i,j)
        enddo
     enddo
     !
     ! Convert to fractions
     write(message,'(''Number of sections with GSD: '',i3)') nptsp
     call write_out(message)
     do i=1,nptsp
        write(message,'(''Section: '',a,'' Sum: '',f9.5,'' (should be 100)'')')&
             labgsp(i),sum(fsj(i,:))
        call write_out(message)
     enddo
     fsj = fsj/100.  ! Convert to mass fraction
     !
     close(ngsp)
     !
     !
     !  CONSISTENCY TEST
     !
     ! Check whether two GSD have the same section number
     do i=1,nptsp-1
        do j=i+1,nptsp
           if(labgsp(i) == labgsp(j)) then
              write(message,'(''ERROR: Two or more spectra have the same &
                   &section number:'',a, '' in file '',a)') trim(labgsp(i)),&
                   trim(params%ground_gsd_file)
              call abort_program(message)
           endif
        enddo
     enddo
     ! Check consistency between spectra and sections
     do i=1,nptsp  ! Loop on the ground spectra
        ifound=0
        do j=1,npts ! Loop on the ground points
           if(labgsp(i) == labgrd(j)) then
              indss(i)=j
              ifound=1
              exit
           endif
        enddo
        if(ifound == 0) then
           write(message,'(''Cannot find section corresponding to GSD='',a)') trim(labgsp(i))
           call abort_program(message)
        endif
     enddo
  endif
  !
  ! To compute weights of ground sections, guess GSD for ground
  ! sections not listed in file ground_grain_size.inp
  ! Here: assume that the GSD is equal to that of the nearest
  ! point listed in ground_grain_size.inp
  !
  if((params%tgsd_method .equal. 'SEARCH') .and. params%use_nearest_points) then
     ntotsp=nptsp
     do i=1,npts
        kmin=1
        indmin = indss(kmin)
        dismin=(xg(1,i)-xg(1,indmin))**2+(xg(2,i)-xg(2,indmin))**2
        do k=1,nptsp
           ind = indss(k)
           if(ind == i) goto 25 ! Already have GSD
           dist = (xg(1,i)-xg(1,ind))**2+(xg(2,i)-xg(2,ind))**2
           if(dist < dismin) then
              dismin = dist
              kmin=k
              indmin=ind
           endif
        enddo
        ntotsp=ntotsp+1     ! Guessed spectra are located in fsj(i,j)
        indss(ntotsp)=i     ! starting from i=nptsp+1
        do j=1,ntypes
           fsj(ntotsp,j) = fsj(kmin,j)
        enddo
25      continue
     enddo                  ! After this loop ntotsp is equal to npts
  endif
  !
  ! Allocate memory
  allocate(srcmas(nsrcmax))
  allocate(b(nsrcmax,ntypes))
  allocate(rex(nsrcmax,ntypes))
  allocate(rey(nsrcmax,ntypes))
  allocate(rmoss(ntypes))
  allocate(rmmod(ntypes))
  allocate(include_class(ntypes))
  allocate(vvj(0:nzlev,ntypes))
  allocate(out(npts,ntypes))
  !
  ! Computes settling velocity in the layers
  !
  write(message,'(''Settling velocity model: '',a)') trim(vset_model_name(params%vmodel))
  call write_out(message)
  !
  do i=0,nzlev
     !
     if(params%vset_of_z == 0) then  ! Settling velocity evaluated at sea level
        rhoa = rhoa0
        pres = pres0
        temp = temp0
     elseif(params%vset_of_z == 1) then  ! Vsettl. is a function of Z (Standard atmosphere)
        call atmosphere(zeta(i),sigma,delta,theta)
        rhoa = sigma*rhoa0
        pres = delta*pres0
        temp = theta*temp0
     elseif(params%vset_of_z == 2) then  ! Vsettl. is a function of Z (use fvcorr)
        rhoa = rhoa0
        pres = pres0
        temp = temp0
     else
        call abort_program('Invalid flag SETTLING_FUNCTION_OF_Z in the input file')
     endif
     call sutherland(temp,visca)
     do j=1,ntypes
        call vsettl(pdiam(j),pdens(j),rhoa,visca,vvj(i,j),params%vmodel,ppsi(j),ierr)
        if(ierr == 1) then
           call abort_program('Invalid settling velocity model')
        elseif(ierr == 2) then
           write(message,'(''***Error in subroutine vsettl'')')
           call write_out(message)
           write(message,'(''Convergence not reached'')')
           call write_out(message)
           write(message,'(''Diameter  ='',1x,e12.5)') pdiam(j)
           call write_out(message)
           write(message,'(''Density   ='',1x,e12.5)') pdens(j)
           call write_out(message)
           write(message,'(''Shape fact='',1x,e12.5)') ppsi(j)
           call write_out(message)
           write(message,'(''Air dens  ='',1x,e12.5)') rhoa
           call write_out(message)
           write(message,'(''Air visc  ='',1x,e12.5)') visca
           call write_out(message)
           write(message,'(''Vset model='',1x,i2)') params%vmodel
           call abort_program('STOP')
        endif
     enddo
  enddo
  !
  ! Recompute vsettling(Z) if use fvcorr
  if(params%vset_of_z == 2) then
     do i=1,nzlev
        do j=1,ntypes
           vvj(i,j) = vvj(0,j)*fvcorr(vvj(0,j),zeta(i))
        enddo
     enddo
  endif
  !
  if(params%suzmodel .equal. 'integrated') then
     call write_out('Suzuki model: integrated')
  else if(params%suzmodel .equal. 'classical') then
     call write_out('Suzuki model: classical')
  else
     call abort_program('Invalid Suzuki model:',params%suzmodel)
  end if
  !
  ! Initialize wground
  wground = 1.0
  !
  !     Generate convex hull of the ground section points
  !
  call convex(npts,xg,npts,in,iwork1,iwork2,ih,nhull,il)
  ! Copy the convex hull in vector (xhull,yhull)
  ik=il(1)
  do i=1,nhull
     j=ih(ik)
     xhull(i) = xg(1,j)
     yhull(i) = xg(2,j)
     labhul(i) = labgrd(j)
     ik=il(ik)
  enddo
  !
  ! Enlarge convex hull (if request)
  if(params%enlarge_convex_hull) call enlarge(nhull,xhull,yhull,params%belthull)
  !
  ! Select method for handling settling velocity classes
  if(params%check_convex_hull) then
     ! Check if ground points are inside hull
     if(params%tgsd_method .equal. 'SEARCH') then
        write(message,'(''Check for maxima inside convex hull: YES'')')
        call write_out(message)
        write(message,'(''List of vertices of the convex hull'')')
        call write_out(message)
        if(params%enlarge_convex_hull) then
           do i = 1, nhull
              write(message,'(a6,1x,(2(1x,f9.1)),2x,''(enlarged convex hull)'')') &
                   trim(labhul(i)), params%xvent+xhull(i),params%yvent+yhull(i)
              call write_out(message)
           enddo
        else
           do i=1,nhull
              write(message,'(a,1x,(2(1x,f9.1)))') trim(labhul(i)),   &
                   params%xvent+xhull(i), params%yvent+yhull(i)
              call write_out(message)
           enddo
        endif
     else if(params%tgsd_method .equal. 'INPUT') then
        include_class = .true.  ! Include all particles
        write(message,'(''WARNING: Check hull N/A when TOTAL_GRAIN_SIZE_METHOD=INPUT (skipped)'')')
        call write_out(message)
     endif
  else
     include_class = .true.  ! Include all particles
     if(params%tgsd_method .equal. 'SEARCH') then
        write(message,'(''Check for maxima inside convex hull: NO'')')
        call write_out(message)
     endif
  endif

  !
  ! BEST FIT
  !

  ! Set lower and upper bounds of the parameters
  allocate(par(npar))
  allocate(parm(npar))
  allocate(low(npar))
  allocate(up(npar))
  low(1) = params%suz1min    ! Suzuki coefficient 1
  up(1)  = params%suz1max
  low(2) = params%suz2min    ! Suzuki coefficient 2
  up(2)  = params%suz2max
  low(3) = params%hcolmin    ! Column height
  up(3)  = params%hcolmax
  low(4) = params%cdmin      ! Diffusion coefficient
  up(5)  = params%cdmax
  low(5) = params%wndmin     ! Wind intensity
  up(5)  = params%wndmax
  low(6) = params%dirmin     ! Wind direction
  up(6)  = params%dirmax

  ! Check the ranges and eventually correct
  do i = 1, npar
     if(up(i) < low(i)) call swap_numbers(low(i), up(i))
     if(up(i) == low(i)) up(i) = low(i)*(1d0 + epsilon(1d0)) + epsilon(1d0)
  end do

  ! Initial estimate of the parameters
  par = 0.5d0*(up + low)
  rchi2m = huge(1d0)
  if(scalepar) then
     rhobeg = 0.5d0*params%initial_diameter
  else
     rhobeg = 0.5d0*params%initial_diameter*minval(abs(up-low))
  end if

  ! Find best fit
  write(message,'(''Optimization method: '',a)') trim(params%optim_method)
  call write_out(message)
  select case (params%optim_method)
  case ('BOBYQA')
     ! Use BOBYQA
     if(params%read_wind_file) call abort_program('Cannot read winds from file with BOBYQA')
     ! Set npt between (npar+2) and (npar+1)*(npar+2)/2
     npt = (npar+1)*(npar+2)/2
     call bobyqa(misfit_subr, npar, npt, par, low, up, &
          rhobeg, 0.5d0*params%minimum_diameter, iprint, params%maxfun, scalepar, status)
     if(status /= 0) call abort_program('Error from bobyqa')
     write(message,'(''Number of tests: '',i7)') params%maxfun
     call write_out(message)
  case ('DIRECT')
     ! Use DIRECT
     if(params%read_wind_file) call abort_program('Cannot read winds from file with DIRECT')
     max_iter = 0  ! Clear (used by vtdirect)
     params%maxfun = 0
     call vtdirect(npar, low, up, misfit_function, par, &
          funmin, status, min_dia=params%minimum_diameter, max_evl=params%maxfun, max_iter=max_iter)
     if(status/10 /= 0) call abort_program('Exit from VTDIRECT with error:', status)
     !if(status /= 0) then
     !   write(message,'(''Exit code from VTDIRECT: '',i3)') status
     !   call write_out(message)
     !end if
     write(message,'(''Number of tests: '',i7)') params%maxfun
     call write_out(message)
  case ('LOOPS')
     ! Brute force (nested loops)
     if(.not.params%read_wind_file) then
        ntest = (nsuz1+1)*(nsuz2+1)*(nhcol+1)*(ncd+1)*(nwnd+1)*(ndir+1)
     else
        ntest = (nsuz1+1)*(nsuz2+1)*(nhcol+1)*(ncd+1)*nwnd
     end if
     write(message,'(''Number of tests: '',i9)') ntest
     call write_out(message)
     do isuz1 = 0, nsuz1
        suz1 = params%suz1min + isuz1*params%suz1step
        do isuz2 = 0, nsuz2
           suz2 = params%suz2min + isuz2*params%suz2step
           do ihcol = 0, nhcol
              hcol = params%hcolmin + ihcol*params%hcolstep
              do iwnd = 0, nwnd
                 if(.not.params%read_wind_file) then
                    wnd = params%wndmin + iwnd*params%wndstep ! Wind at tropopause
                 else
                    ! Get wind profile iwnd from the winds list
                    if(iwnd == 0) cycle
                    call hazwinds_get(winds, iwnd, windx, windy, wdate)
                 end if
                 do idir = 0, ndir ! Loop on wind direction
                    if(.not.params%read_wind_file) then
                       dir = params%dirmin + idir*params%dirstep
                       ! Generate wind profile (from prototype)
                       call genwind(nzlev,wnd,dir,wvel,windx,windy)
                    end if
                    do icd = 0, ncd
                       cdiff = params%cdmin + icd*params%cdstep
                       ! Generate parameter vector
                       par(1) = suz1
                       par(2) = suz2
                       par(3) = hcol
                       par(4) = cdiff
                       par(5) = wnd
                       par(6) = dir
                       ! Call the misfit function
                       rchi2 = misfit_function(npar, par, status)
                       ! Check for minimum chi2 and save
                       if(rchi2 < rchi2m) then
                          parm = par
                          rchi2m = rchi2
                          iwndm = iwnd   ! For winds read from file
                       end if
                    end do
                    if(params%read_wind_file) exit ! Exit loop on directions
                 end do
              end do
           end do
        end do
     end do
     ! Get best parameters
     par = parm
  case default
     call abort_program('Invalid optimization method:', params%optim_method)
  end select

  ! Call again for calculating chi2
  rchi2 = misfit_function(npar, par, status)

  !
  ! WRITE OUTPUT FILE
  !
  write(message,'(''CHI2('',i1,''):  '',g12.5)') params%modew, rchi2
  call write_out(message)
  write(message,'(''TOTMASS:  '',e12.5)') beta
  call write_out(message)
  write(message,'(''VOL(DRE): '',g12.5,'' (km3)'')') beta*1E-9/rhodre
  call write_out(message)
  if(.not.params%read_wind_file) then
     write(message,'(''VEL-WIND: '',f7.3)') wnd
     call write_out(message)
     write(message,'(''DIR-WIND: '',f7.3)') deg(dir)
     call write_out(message)
  end if
  write(message,'(''HCOL:     '',f7.1)') hcol
  call write_out(message)
  write(message,'(''CSUZ1:    '',f6.3)') suz1
  call write_out(message)
  write(message,'(''CSUZ2:    '',f6.3)') suz2
  call write_out(message)
  write(message,'(''CDIFF:    '',f7.1)') cdiff
  call write_out(message)
  write(message,'('' Diameter     dens. sphe.   wt%     # P-TYPE  Vset (at sea level)'')')
  call write_out(message)
  !
  sumvs = 0.0
  do j = 1, ntypes
     if(include_class(j)) then
        if(100.*fp(j) >= 1e-4) then
           sumvs = sumvs + 100.*fp(j)          ! Convert fractions to wt%
           write(message,110) pdiam(j),pdens(j),ppsi(j),100.*fp(j),j,vvj(0,j)
           call write_out(message)
        end if
     endif
  enddo
  write(message,111) sumvs
  call write_out(message)

110 format(e12.5,1x,f6.1,1x,f5.3,1x,f8.4,2x,'#',2x,i4,1x,g12.5)
111 format('SUM:',1x,f7.3)
  !
  ! Write comparison file
  call get_free_unit(ncmp)
  if(ncmp < 0) call abort_program('Cannot get file unit (ncmp)')
  open(ncmp, file=fcmp, status='unknown', err=2007)
  write(ncmp,'(''# Label     X        Y        obs_load     sim_load     dist_vent'')')
  do i=1,npts
     write(ncmp,120) labgrd(i), params%xvent+xg(1,i), params%yvent+xg(2,i),ground(i),&
          totout(i),sqrt(xg(1,i)**2+xg(2,i)**2)
  enddo
120 format(a6,1x,f10.2,1x,f10.2,1x,e12.5,1x,e12.5,1x,f10.2)
  close(ncmp)
  write(nterm,*) 'Wrote cmp    file (ASCII): '//trim(fcmp)
  !
  ! Write wind file
  if(.not.params%read_wind_file) then
     call genwind(nzlev,wnd,dir,wvel,windx,windy)
     call wriwind(fwnd,nzlev,zeta,windx,windy,wdate)
  else
     call hazwinds_write(winds, fwnd, iwndm)
  end if
  !
  write(nterm,*) 'Wrote wind   file (ASCII): '//trim(fwnd)
  !
  !     Compute positions of gaussian centers of mass
  !
  call parcumula
  !
  nsrc = nint((hcol-params%zvent)/params%hlayer)
  call suzuki (srcmas,nsrc,suz1,suz2)
  write(message,'(5x,''PT Vset(sealev)'',7x,''X_mass'',6x,''Y_mass'',4x,''Distance'')')
  call write_out(message)
  do j=1,ntypes
     xmass = 0.
     ymass = 0.
     do i=1,nsrc
        fmass = srcmas(i)
        xmass = xmass + fmass*rex(i,j)
        ymass = ymass + fmass*rey(i,j)
     enddo
     ! Distance from the vent
     dist = sqrt(xmass**2+ymass**2)
     !
     ! Georeference the center of mass
     xmass = xmass + params%xvent
     ymass = ymass + params%yvent
     !
     if(params%check_convex_hull) then
        if(include_class(j)) then
           write(message,'(''IN '',i3,3x,g12.5,3(1x,f12.1))') &
                j,vvj(0,j),xmass,ymass,dist
           call write_out(message)
        else
           write(message,'(''OUT'',i3,3x,g12.5,3(1x,f12.1))') &
                j,vvj(0,j),xmass,ymass,dist
           call write_out(message)
        endif
     else
        write(message,'(3x,i3,3x,g12.5,3(1x,f12.1))') &
             j,vvj(0,j),xmass,ymass,dist
        call write_out(message)
     endif
  enddo

  close(nout)
  write(nterm,*) 'Wrote output file (ASCII): '//trim(fout)
  if(params%write_chi2) then
     close(nchi)
     write(nterm,*) 'Wrote output file (ASCII): '//trim(fchi)
  endif
  !
  call normal_exit
  !
  !     Errors
  !
2003 call abort_program('Cannot open file:', params%ground_thickness)
2004 call abort_program('Cannot open file:', fout)
2005 call abort_program('Cannot open file:', params%ground_gsd_file)
2006 call abort_program('Cannot open file:', params%tgsd_file)
2007 call abort_program('Cannot open file:', fcmp)
2008 call abort_program('Error reading file:', params%ground_thickness)

contains

  !> Misfit subroutine: it is called by subroutine BOBYQA
  subroutine misfit_subr(n, par, chi2)
    implicit none
    integer :: n
    real(8) :: par(:)
    real(8) :: chi2
    integer :: iflag
    chi2 = misfit_function(n, par, iflag)
  end subroutine misfit_subr

  !> Misfit function: it is called by subroutine VTdirect or misfit_subr
  !! Evaluates the chi2 given the model parameters
  function misfit_function(n, par, status) result(rchi2)
    implicit none
    integer, intent(in)  :: n
    real(8), intent(in)  :: par(:)
    integer, intent(out) :: status
    real(8) :: rchi2
    integer :: inside
    status = 0
    suz1 = par(1)
    suz2 = par(2)
    hcol = par(3)
    cdiff= par(4)
    wnd  = par(5)
    dir  = par(6)
    !
    nsrc = nint((hcol-params%zvent)/params%hlayer)
    srcstep = (hcol-params%zvent)/nsrc
    do j=1,nsrc
       zsrc(j) = params%zvent+j*srcstep ! Used by subroutine parcumula
    enddo
    if(params%suzmodel .equal. 'integrated') then
       ! Integrated suzuki
       call isuzuki(srcmas, nsrc, suz1, suz2)
    else if(params%suzmodel .equal. 'classical') then
       ! Classical suzuki (not-integrated)
       call suzuki(srcmas, nsrc, suz1, suz2)
    else
       call abort_program('Invalid Suzuki model:', params%suzmodel)
    end if

    ! Generate wind profile (from prototype)
    if(.not.params%read_wind_file) call genwind(nzlev, wnd, dir, wvel, windx, windy)

    ! Find center of mass of gaussian deposits
    call parcumula
    !
    ! Estimate settling velocity distribution
    if(params%tgsd_method .equal. 'SEARCH') then ! Use file ground_grain_size.inp
       fmin  = 0.
       fmout = 0.
       nctypes = 0
       do j=1,ntypes
          xmass = 0.
          ymass = 0.
          do i=1,nsrc
             fmass = srcmas(i)
             xmass = xmass + fmass*rex(i,j)
             ymass = ymass + fmass*rey(i,j)
          enddo
          ! Check if deposit maxima are inside the perimeter
          ! (convex hull) of the ground points
          if(params%check_convex_hull) then
             ins=inside(nhull, xhull, yhull, xmass, ymass)
             if(ins == 1) then
                include_class(j) = .false. ! Exclude this particle type
                fmout = fmout + fp(j)
             else
                include_class(j) = .true. ! Include this particle type
                fmin = fmin + fp(j)
                nctypes = nctypes + 1
             endif
          else
             nctypes = nctypes + 1
          endif
       enddo
    else  ! Spectra from parfit.inp
       nctypes = ntypes
    endif
    !
    ! Compute deposit in the given sections for p-type
    call depclas(npts,xg,out)
    !
    ! Estimate mass in the column for each particle type
    if(params%tgsd_method .equal. 'SEARCH') then  ! Use file ground_grain_size.inp
       do j=1,ntypes
          rmoss(j) = 0.
          rmmod(j) = 0.
          do i=1,nptsp
             if(include_class(j)) then
                ind = indss(i)
                rmoss(j)=rmoss(j) + ground(ind)*fsj(i,j)
                rmmod(j)=rmmod(j) + totmm*out(ind,j)
             endif
          enddo
       enddo
       !
       ! Fit grain size distribution
       rmost = 0.
       do j=1,ntypes
          if(include_class(j)) then
             fp(j) = safediv(rmoss(j),rmmod(j))
             rmost = rmost + fp(j)
          endif
       enddo
       !
       ! Normalize
       do j=1,ntypes
          if(include_class(j)) then
             fp(j) = safediv(fp(j),rmost)
          endif
       enddo
    endif
    !
    ! Compute total mass
    do i=1,npts
       totout(i) = 0.
       do j=1,ntypes
          if(include_class(j)) then
             totout(i) = totout(i) + out(i,j)*fp(j)
          endif
       enddo
    enddo
    !
    ! Compute weigths of ground sections
    ! (wgrounds were initialized=1)
    if((params%tgsd_method .equal. 'SEARCH') .and. params%use_nearest_points)then
       do i=1,ntotsp
          ind = indss(i)
          wground(ind)=0.
          do j=1,ntypes
             if(include_class(j)) then
                wground(ind)=wground(ind)+fsj(i,j)
             endif
          enddo
       enddo
    endif
    !
    ! Normalize deposit (best fit with total mass)
    cout = 0d0
    cgrd = 0d0
    do i = 1, npts
       cout = cout + totout(i)
       cgrd = cgrd + ground(i)*wground(i)
    enddo
    !
    beta = safediv(cgrd, cout, status=status)  ! Set status=1 if beta=undefined
    !
    do i=1,npts
       totout(i) = beta*totout(i)
    enddo
    !
    ! Compute chi2
    call chi2(rchi2, npts, totout, ground, weight)

    if(params%write_chi2) then
       write(nchi,80) suz1,suz2,hcol,wnd,deg(dir), &
            cdiff,beta,rchi2,nctypes
80     format(8(1x,e12.5),1x,i3)
    endif

  end function misfit_function

  !> Swap a and b
  subroutine swap_numbers(a, b)
    implicit none
    real(8), intent(inout) :: a, b
    real(8) :: tmp
    tmp = a
    a   = b
    b   = tmp
  end subroutine swap_numbers

end program parfit
