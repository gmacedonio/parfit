!> Warning and error messages
module m_errors
  !
  ! Author: Giovanni Macedonio
  !
  ! Version: 1.2
  !
  ! Date of first version: 28-FEB-2017
  ! Date of this  version: 26-JUL-2024
  !
  ! Note: If the compiler does not support the intrinsic subroutine exit,
  ! use exit.f90
  !
  implicit none
  integer, private :: nlog=6  ! Unit for log messages
  integer, private :: nerr=6  ! Unit for error messages

  interface write_log
     module procedure write_log_msg
     module procedure write_log_msg_string
  end interface write_log

  interface abort_program
     module procedure abort_no_msg
     module procedure abort_with_msg
     module procedure abort_with_msg_string
     module procedure abort_with_msg_integer
  end interface abort_program

contains

  !> Set the file unit for log messages
  subroutine set_log_unit(unit)
    implicit none
    integer, intent(in) :: unit
    nerr=unit
  end subroutine set_log_unit

  !> Set the file unit for the error messages
  subroutine set_error_unit(unit)
    implicit none
    integer, intent(in) :: unit
    nerr=unit
  end subroutine set_error_unit

  !> Write a message in the log
  subroutine write_log_msg(msg)
    implicit none
    character(len=*), intent(in) :: msg
    write(nlog,'(a)') trim(adjustl(msg))
  end subroutine write_log_msg

  !> Write a message in the log and a string
  subroutine write_log_msg_string(msg,string)
    implicit none
    character(len=*), intent(in) :: msg
    character(len=*), intent(in) :: string
    write(nlog,'(a,1x,a)') trim(adjustl(msg)),trim(adjustl(string))
  end subroutine write_log_msg_string

  !> Abort the program with no message
  subroutine abort_no_msg
    implicit none
    call force_exit(1)
  end subroutine abort_no_msg

  !> Send a message and abort the program
  subroutine abort_with_msg(msg)
    implicit none
    character(len=*), intent(in) :: msg
    write(nerr,'(a)') trim(adjustl(msg))
    call force_exit(1)
  end subroutine abort_with_msg

  !> Send a message+string and abort the program
  subroutine abort_with_msg_string(msg,string)
    implicit none
    character(len=*), intent(in) :: msg
    character(len=*), intent(in) :: string
    write(nerr,'(a,1x,a)') trim(adjustl(msg)),trim(adjustl(string))
    call force_exit(1)
  end subroutine abort_with_msg_string

  !> Send a message+integer and abort the program
  subroutine abort_with_msg_integer(msg,number)
    implicit none
    character(len=*), intent(in) :: msg
    integer, intent(in) :: number
    write(nerr,'(a,1x,i7)') trim(adjustl(msg)),number
    call force_exit(1)
  end subroutine abort_with_msg_integer

  !> Exit the program
  subroutine normal_exit
    call force_exit(0)
  end subroutine normal_exit

  !> Exit the program
  subroutine force_exit(code)
    integer, intent(in) :: code
    call exit(code)
  end subroutine force_exit

end module m_errors
