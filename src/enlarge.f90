subroutine enlarge(np,xp,yp,tk)
  !
  ! Modifies the positions of a list of points along a convex line in
  ! such a way that the new points define a curve at a distance tk from
  ! the previous line
  !
  ! The point (xp,yp) along the curve must be listed counterclockwise
  !
  use m_safeop
  implicit none
  integer :: np
  real(8) :: xp(np),yp(np),tk
  real(8), allocatable :: xold(:),yold(:)    ! The new points
  integer :: i
  real(8) :: a1,a2,b1,b2,aq,bq,tq
  !
  ! Allocate memery fo the new points
  allocate(xold(0:np+1))
  allocate(yold(0:np+1))
  !
  ! Copy vectors
  xold(1:np) = xp(1:np)
  yold(1:np) = yp(1:np)
  !
  xold(0) = xp(np)
  yold(0) = yp(np)
  xold(np+1) = xp(1)
  yold(np+1) = yp(1)
  !
  do i=1,np
     a1 = xold(i)-xold(i-1)  ! Direction of first line
     b1 = yold(i)-yold(i-1)
     a2 = xold(i)-xold(i+1)  ! Direction of second line
     b2 = yold(i)-yold(i+1)
     ! Normalize
     call normalize(a1,b1)   ! Normalize
     call normalize(a2,b2)   ! Normalize
     !
     aq = a1+a2              ! Direction of new point
     bq = b1+b2
     call normalize(aq,bq)
     !
     tq = tk/(b1*aq-a1*bq)
     xp(i) = xold(i) + aq*tq  ! Coordinates of the new point
     yp(i) = yold(i) + bq*tq
  enddo
  !
  deallocate(xold)
  deallocate(yold)

end subroutine enlarge
