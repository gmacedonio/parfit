module m_safeop
  ! Module for safe operations
  !
  ! Provides:
  ! function safediv(a,b)
  ! subroutine normalize(a,b)
  !
  implicit none

contains

  real(8) function safediv(a,b, status)
    ! Divide two numbers if denominator is not small, else returns zero and
    ! the optional status flag is set to 1
    !
    implicit none
    real(8), intent(in)  :: a,b
    integer, optional, intent(out) :: status
    real(8), parameter :: RMIN   = TINY(1d0)
    if(abs(b) >= RMIN) then
       safediv = a/b
       if(present(status)) status = 0
    else
       safediv = 0d0
       if(present(status)) status = 1
    end if
  end function safediv

  subroutine normalize(a,b)
    ! Normalize the components of a 2D non-null vector
    implicit none
    real(8), intent(inout) :: a,b
    real(8) :: norm
    norm = sqrt(a**2+b**2)
    a = safediv(a,norm)
    b = safediv(b,norm)
  end subroutine normalize

end module m_safeop
