module airprop
  ! Properties of the atmosphere and reference values at sea level
  !
  ! Author: G.Macedonio
  ! INGV, Osservatorio Vesuviano, Napoli, Italy
  ! e-mail: macedon@ov.ingv.it
  !
  ! Version : 1.0
  !
  ! Date of first version:  9-NOV-2009
  ! Date of this  version:  9-NOV-2009
  !
  implicit none
  !
  real(8), parameter :: airgamma = 1.4d0      ! Cp/Cv for atmosphere
  real(8), parameter :: runiv = 8.314472d0    ! Universal gas constant
  real(8), parameter :: pmola = 28.966d-3     ! Molecular weight of air
  !
  real(8), parameter :: pres0 = 101325.0d0    ! Ref. pressure at sea level
  real(8), parameter :: temp0 = 288.16d0      ! Ref. temperatrure at sea level
  real(8), parameter :: rhoa0 = 1.22500d0     ! Air density at sea level
  !
end module airprop
