integer function inside (np,xl,yl,x,y)
  !     This routine checks if a point is inside a closed line
  !
  !     Return values:
  !     .       -1 = internal
  !     .        0 = on the border
  !     .        1 = external
  !
  !     np  - number of points in the closed line
  !     xl,yl - coodinates of the points of the closed line
  !     x,y - coordinates of the point to be checked
  !
  !     If the line is not close, it is closed automatically (by connecting
  !     the last point with the first one)
  !
  !     Author: G.Macedonio
  !     Istituto Nazionale di Geofisica e Vulcanologia
  !     Osservatorio Vesuviano, Napoli, Italy
  !
  implicit none
  !
  integer, intent(in)  :: np
  real(8), intent(in)  :: xl(np),yl(np)
  real(8), intent(out) :: x,y
  logical :: even
  integer :: i,i1,i2,icross,iflag
  real(8) :: rn,rm1,rm2
  !
  inside = 0
  icross = 0
  do i=1,np
     i1 = i
     i2 = i+1
     if(i==np) i2 = 1
     iflag = 0
     if (yl(i2) /= yl(i1)) then
        if (yl(i2) >= y) iflag = iflag+1
        if (yl(i1) < y) iflag = iflag+1
        if (even(iflag)) then
           rn=y*(xl(i2)-xl(i1))+xl(i1)*(yl(i2)-yl(i1))-(xl(i2)-xl(i1))*yl(i1)
           rm1=(yl(i2)-yl(i1))
           rm2=rm1*x
           if (rn == rm2) return
           if (rm1 > 0.0 .and. rn > rm2 .or. rm1 < 0.0 .and. rn < rm2) &
                icross = icross+1
        endif
     else
        if (yl(i1)==y) then
           if (xl(i2) >= x) iflag = iflag+1
           if (xl(i1) <= x) iflag = iflag+1
           if (even(iflag)) return
        endif
     endif
  enddo
  if(even(icross)) then
     inside=1
  else
     inside = -1
  endif
  return
end function inside
!
logical function even (iarg)
  !
  !     This function returns true if its argument is even
  !
  implicit none
  integer :: iarg
  even = .false.
  if (mod(iarg,2)==0) even = .true.
  return
end function even
