PARFIT is a tool for the parametrization of volcanic ash deposits
See file parfit-manual.pdf in the doc directory for more information.

If you install from git you need to create the autotools script 'configure'
To do this, you need to issue the command: autoreconf -i

This command is not needed if you install from the parfit tarball distributed
at http://datasim.ov.ingv.it/parfit.html


NOTE:
Parfit uses the following public routines:

*) To define the properties of the U.S. Standard Atmosphere 1977, parfit
   uses subroutine atmosphere written by Ralph Carmichael, Public Domain
   Aeronautical Software, 2009 (freely available at http:///www.pdas.com).

*) To define the convex hull of the sampled ground points, parfit uses
   algorithm 523 by W.F. Eddy, ACM TOMS 3 (1977) 411-412, available at
   URL http://www.netlib.org (TOMS, algorithm 523).
   Here, algorithm 523 was translated in F90 and is contained into
   subroutines convex.f90 and split.f90

*) The optimization method is based on a modified version of two external
   routines: BOBYQA and VTDIRECT. See files src/m_vtdirect.f90 and
   m_bobyqa.f90 for more details.
