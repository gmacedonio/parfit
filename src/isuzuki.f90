subroutine isuzuki (suz,nsrc,asuz1,asuz2)
  !
  !     Mass distribution along the column based on the
  !     integrated Suzuki distribution.
  !
  implicit none
  !
  ! Arguments
  integer, intent(in)  :: nsrc
  real(8), intent(out) :: suz(nsrc)
  real(8), intent(in)  :: asuz1,asuz2
  !
  ! Local variables
  real(8) :: lambdap1,alambda
  real(8) :: igamma
  real(8) :: x,dh
  real(8) :: sum
  integer  :: i
  !
  lambdap1 = 1d0+asuz2   ! lambda+1
  alambda  = asuz1*asuz2    ! A*lambda
  !
  dh=1d0/nsrc       ! Normalized vertical step
  x=1d0-0.5d0*dh   ! Extrema of the layer around the nodes
  !
  ! First pass (cumulative values)
  do i=1,nsrc
     suz(i)=igamma(lambdap1,alambda*x)
     x = x-dh
  enddo
  sum = suz(1)   ! Now this corresponds to the total mass
  !
  ! Second pass (discretize an normalize)
  do i=1,nsrc-1
     suz(i)=(suz(i)-suz(i+1))/sum
  enddo
  suz(nsrc)=suz(nsrc)/sum  ! Normalize last node
  !
end subroutine isuzuki
