subroutine convex(n,x,m,in,ia,ib,ih,nh,il)
  !
  !     Translated to F90 by G.Macedonio
  !
  !     Algorithm 523 by W.F. Eddy, ACM TOMS 3 (1977) 411-412
  !
  !     This subroutine determines which of the m points of array
  !     x whose subscripts are in array in are vertices of the
  !     minimum area convex polygon containing the m points. The
  !     subscripts of the vertices are placed in array ih in the
  !     order they are found. nh is the number of elements in
  !     array ih and array il. Array il is a linked list giving
  !     the order of the elements of array ih in a counter
  !     clockwise direction. This algorithm corresponds to a
  !     preorder traversal of a certain binary tree. Each vertex
  !     of the binary tree represents a subset of the m points.
  !     At each step the subset of points corresponding to the
  !     current vertex of the tree is partitioned by a line
  !     joining two vertices of the convex polygon. The left son
  !     vertex in the binary tree represents the subset of points
  !     above the partitioning line and the right son vertex, the
  !     subset below the line. The leaves of the tree represent
  !     either null subsets or subsets inside a triangle whose
  !     vertices coincide with vertices of the convex polygon.
  !
  !     Formal parameters
  !
  !     Input
  !     n  integer           total number of data points
  !     x  real array (2,n)  (x,y) co-ordinates of the data
  !     m  integer           number of points in the input subset
  !     in integer array (m) subscripts for array x of the points
  !     .  in the input subset
  !
  !     Work area
  !     ia integer array (m) subscripts for array x of left son
  !     .  subsets. see comments after dimension statements
  !     ib integer array (m) subscripts for array x of right son
  !     .  subsets
  !
  !     Output
  !     ih integer array (m) subscripts for array x of the
  !        vertices of the convex hull
  !     nh integer number of elements in array ih and array il. same
  !        as number of vertices of the convex polygon
  !     il integer array (m) a linked list giving in order in a
  !        counter-clockwise direction the elements of array ih
  !
  implicit none
  integer :: n,m
  real(8) :: x(2,n)
  integer :: in(m),ia(m),ib(m),ih(m),il(m)
  !     The upper end of array ia is used to store temporarily
  !     the sizes of the subsets which correspond to right son
  !     vertices, while traversing down the left sons when on the
  !     left half of the tree, and to store the sizes of the left
  !     sons while traversing the right sons(down the right half)
  !
  logical :: maxe,mine
  integer :: kn,kx,mp1,mx,mini,mm,mb,nh,inh,nib,ma,nia,mxb,mbb,ilinh,mxa,mxbb
  integer :: i,j
  !
  if(m == 1) goto 22
  il(1)=2
  il(2)=1
  kn=in(1)
  kx=in(2)
  if(m==2) goto 21
  mp1=m+1
  mini=1
  mx=1
  kx=in(1)
  maxe=.false.
  mine=.false.
  !     find two vertices of the convex hull for the initial
  !     partition
  do i=2,m
     j=in(i)

     if(x(1,j)-x(1,kx) > 0.) then
        maxe=.false.
        mx=i
        kx=j
     elseif(x(1,j)-x(1,kx) == 0.) then
        maxe=.true.
     endif
     if(x(1,j)-x(1,kn) < 0.) then
        mine=.false.
        mini=i
        kn=j
     elseif(x(1,j)-x(1,kn) == 0.) then
        mine=.true.
     endif

  enddo
  !
  !     If the max and min are equal, all m points lie on a
  !     vertical line
  if(kx == kn) goto 18
  !
  !     If maxe (or mine) has the value true there are several
  !     maxima (or minima) with equal first coordinates
  if(maxe.or.mine) goto 23
7 ih(1)=kx
  ih(2)=kn
  nh=3
  inh=1
  nib=1
  ma=m
  in(mx)=in(m)
  in(m)=kx
  mm=m-2
  if(mini == m) mini=mx
  in(mini)=in(m-1)
  in(m-1)=kn
  !
  !     Begin by partitioning the root of the tree
  call split(n,x,mm,in,ih(1),ih(2),0,ia,mb,mxa,ib,ia(ma),mxbb)
  !
  !     First traverse the left half of the tree
  !     start with the left son
8 nib=nib+ia(ma)
  ma=ma-1
9 if(mxa==0) goto 11
  il(nh)=il(inh)
  il(inh)=nh
  ih(nh)=ia(mxa)
  ia(mxa)=ia(mb)
  mb=mb-1
  nh=nh+1
  if(mb==0) goto 10
  ilinh=il(inh)
  call split(n,x,mb,ia,ih(inh),ih(ilinh),1,ia,mbb,mxa,ib(nib),ia(ma),mxb)
  mb=mbb
  goto 8
  !
  !     Then the right son
10 inh=il(inh)
11 inh=il(inh)
  ma=ma+1
  nib=nib-ia(ma)
  if(ma >= m) goto 12
  if(ia(ma)==0) goto 11
  ilinh=il(inh)
  !
  !     On the left side of the tree, the right son of a right son
  !     must represent a subset of points which is inside a
  !     triangle with vertices which are also vertices of the
  !     convex polygon and hence the subset may be neglected.
  call split(n,x,ia(ma),ib(nib),ih(inh),ih(ilinh),2,ia,mb,mxa,ib(nib),mbb,mxb)
  ia(ma)=mbb
  goto 9
  !
  !     Now traverse the right half of the tree
12 mxb=mxbb
  ma=m
  mb=ia(ma)
  nia=1
  ia(ma)=0
  !
  !     Start with the right son
13 nia=nia+ia(ma)
  ma=ma-1
14 if(mxb == 0) goto 16
  il(nh)=il(inh)
  il(inh)=nh
  ih(nh)=ib(mxb)
  ib(mxb)=ib(mb)
  mb=mb-1
  nh=nh+1
  if(mb == 0) goto 15
  ilinh=il(inh)
  call split(n,x,mb,ib(nib),ih(inh),ih(ilinh),-1,ia(nia),    &
       ia(ma),mxa,ib(nib),mbb,mxb)
  mb=mbb
  goto 13
  !
  !     Then the left son
15 inh=il(inh)
16 inh=il(inh)
  ma=ma+1
  if(ma==mp1) goto 17    ! This line was moved up by G.Macedonio
  nia=nia-ia(ma)
  if(ia(ma)==0)goto 16
  ilinh=il(inh)
  !
  !     On the right side of the tree, the left son of a left son
  !     must represent a subset of points which is inside a
  !     triangle with vertices which are also vertices of the
  !     convex polygon and hence the subset may be neglected.
  !
  call split(n,x,ia(ma),ia(nia),ih(inh),ih(ilinh),-2,ia(nia),mbb,  &
       mxa,ib(nib),mb,mxb)
  goto 14
17 nh=nh-1
  return
  !
  !     All the special cases are handled down here
  !     if all the points lie on a vertical line
18 kx=in(1)
  kn=in(1)
  do i=1,m
     j=in(i)
     if(x(2,j) <= x(2,kx)) goto 19
     mx=i
     kx=j
19   if(x(2,j) >= x(2,kn)) goto 20
     mini=i
     kn=j
20   continue
  enddo
  if(kx==kn) goto 22
  !
  !     If there are only two points
21 ih(1)=kx
  ih(2)=kn
  nh=3
  if((x(1,kn)==x(1,kx)).and.(x(2,kn)==x(2,kx))) nh=2
  goto 17
  !
  !     If there is only one point
22 nh=2
  ih(1)=in(1)
  il(1)=1
  goto 17
  !
  !     Multiple extremes are handled here
  !     if there are several points with the (same) largest
  !     first coordinate
23 if(.not.maxe) goto 25
  do i=1,m
     j=in(i)
     if(x(1,j) /= x(1,kx)) continue
     if(x(2,j) <= x(2,kx)) continue
     mx=i
     kx=j
  enddo
  !
  !     If there are several points with the (same) smallest
  !     first coordinate
25 if(.not.mine) goto 7
  do i=1,m
     j=in(i)
     if(x(1,j) /= x(1,kn)) continue
     if(x(2,j) >= x(2,kn)) continue
     mini=i
     kn=j
  enddo
  goto 7
end subroutine convex
