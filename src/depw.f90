subroutine depw(npts,ground,weight,modew)
  !
  !     DEPW - Generate weights for chi2
  !
  !     npts    - Number of ground points (input)
  !     ground  - Vector of dimension npts containing ground loadings (input)
  !     weight  - Vector of dimension npts containing weights for chi2
  !     modew   - Selects the type of weights
  !     .         0 - weight=1 (minimum least squares)
  !     .         1 - weight=1/ground**2 (uniform relative error)
  !     .         2 - weight=1/ground (poissonian distribution)
  !
  !
  use m_errors
  implicit none
  !
  integer :: npts,modew
  real(8) :: ground(npts),weight(npts)
  real(8), parameter :: eps = 1.0             ! Maximum weight is one
  real(8) :: sum,rn
  integer :: i
  !
  sum = 0.
  do i = 1,npts
     sum = sum + ground(i)**2
  enddo
  !
  if(modew==0) then
     weight = 1.0/sum
  else if(modew==1) then
     do i=1,npts
        weight(i) = 1./(npts*max(ground(i)**2,eps))
     enddo
  else if(modew==2) then
     rn = sqrt(npts*sum)
     do i=1,npts
        weight(i) = 1./(rn*max(ground(i),eps))
     enddo
  else
     call abort_program('Error: Unknown weighting mode in subroutine depw')
  endif
end subroutine depw
