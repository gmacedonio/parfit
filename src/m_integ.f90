module m_integ
  use m_errors

contains

  subroutine integ(n,xi,dxi,fi,x,ret)
    !     Integrate a function (fi) specified in discrete points (xi)
    !     between 0 and x, using the trapezoid rule.
    !
    !     n   - Number of function points (input)
    !     xi  - Vector of dimension n containing x-coordinates  (input)
    !     dxi - Vector of dimension (n-1) containing x-intervals  (input)
    !     fi  - Vector of dimension n containing values of f(xi)  (input)
    !     x   - Right extremum of integration (input)
    !     ret - Return value (output)
    !
    !     Notes: xi values must be specified in ascending order;
    !     xi(1) must be equal to 0; x cannot be grater than xi(n);
    !     values dxi(i) must be equal to xi(i+1)-xi(i).
    !
    implicit none
    integer :: n
    real(8) :: xi(n),dxi(n),fi(n)
    real(8) :: x
    real(8) :: ret
    real(8) :: dx,fdx
    integer :: i,n1,n2
    !
    do i=n-1,1,(-1)           ! We know that x is < xi(n)
       if(x >= xi(i)) goto 10
    enddo
    write(*,*) 'ERROR in subroutine integ: x < xi(1) ?', x,xi(1)
    call abort_program
10  n1 = i
    n2 = n1+1
    !
    !     First n1 intervals
    ret = 0d0
    do i=1,n1-1
       ret = ret + (fi(i)+fi(i+1))*dxi(i)
    enddo
    !     Integrate last segment
    dx = x-xi(n1)
    fdx = dx/dxi(n1)
    ret = ret + (2d0*fi(n1)+(fi(n2)-fi(n1))*fdx)*dx
    !
    ret = 0.5d0*ret
    return
  end subroutine integ

end module m_integ
