subroutine mindist(np,xlist,ylist,xp,yp,ind,dist)
  !
  !     Find the minimum distance between a fixed point (xp,yp) and the
  !     points in a list
  !
  !     np           - Number of point in the list (input)
  !     xlist, ylist - Vectors of dimension np containing the coordinates
  !     .              of the points in the line (input)
  !     xp,yp        - Coordinates of the fixed point (input)
  !     ind          - Index of the point with minimum distance (output)
  !     dist         - Minimum distance of (xp,yp) from points in the list
  !
  !
  implicit none
  !
  integer :: np,ind
  real(8) :: xlist(np),ylist(np)
  real(8) :: xp,yp,dist
  integer :: i
  real(8) :: tdist
  !
  dist = sqrt((xp-xlist(1))**2+(yp-ylist(1))**2)
  ind=1
  do i=2,np
     tdist = sqrt((xp-xlist(i))**2+(yp-ylist(i))**2)
     if(tdist < dist) then
        ind = i
        dist = tdist
     endif
  enddo
end subroutine mindist
