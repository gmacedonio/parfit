module m_strings
  ! Utilities operations on strings
  !
  ! Author: Giovanni Macedonio
  !
  ! Version: 1.4
  !
  ! Date of first version: 20-MAR-2011
  ! Date of this  version: 05-SEP-2020
  !
  ! Provides:
  ! subroutine convert_to_uppercase(string) - Convert string to uppercase
  ! subroutine convert_to_lowercase(string) - Convert string to lowercase
  ! logical function is_equal(str1,str2)    - Compare two strings
  ! logical function is_blank(char)         - Check if a character is a blank
  ! operator (.equal.)                      - Compare strings
  ! integer function field_count(record)
  ! integer function get_start_of_field(record, nf)

  interface read_from_string
     module procedure :: read_from_string_int
     module procedure :: read_from_string_r4
     module procedure :: read_from_string_r8
  end interface read_from_string

  ! Compare two strings (case insensitive)
  interface operator (.equal.)
     module procedure is_equal
  end interface operator (.equal.)

contains

  subroutine read_from_string_int(string,num,status)
    ! Read an integer number from a string
    ! status: 0=0K, 1=Error
    implicit none
    character(len=*) :: string
    integer, intent(out) :: num
    integer, intent(out) :: status
    status=0
    read(string,*,err=20,end=20) num
    return
20  status=1
  end subroutine read_from_string_int

  subroutine read_from_string_r4(string,num,status)
    ! Read an real(4) number from a string
    ! status: 0=0K, 1=Error
    implicit none
    character(len=*) :: string
    real(4), intent(out) :: num
    integer, intent(out) :: status
    status=0
    read(string,*,err=20,end=20) num
    return
20  status=1
  end subroutine read_from_string_r4

  subroutine read_from_string_r8(string,num,status)
    ! Read an real(8) number from a string
    ! status: 0=0K, 1=Error
    implicit none
    character(len=*) :: string
    integer, intent(out) :: num
    real(8), intent(out) :: status
    status=0
    read(string,*,err=20,end=20) num
    return
20  status=1
  end subroutine read_from_string_r8

  subroutine convert_to_uppercase(str)
    ! Convert a string to uppercase
    implicit none
    character(len=*), intent(inout) :: str
    integer :: i, del
    del = iachar('a') - iachar('A')
    do i = 1, len_trim(str)
       if (lge(str(i:i),'a') .and. lle(str(i:i),'z')) then
          str(i:i) = achar(iachar(str(i:i)) - del)
       end if
    end do
    return
  end subroutine convert_to_uppercase

  subroutine convert_to_lowercase(str)
    ! Convert a string to lowercase
    implicit none
    character(len=*), intent(inout) :: str
    integer :: i, del
    del = iachar('a') - iachar('A')
    do i = 1, len_trim(str)
       if (lge(str(i:i),'A') .and. lle(str(i:i),'Z')) then
          str(i:i) = achar(iachar(str(i:i)) + del)
       end if
    end do
    return
  end subroutine convert_to_lowercase

  logical function is_equal(str1,str2)
    ! Compare two strings (no case sensitive)
    implicit none
    character(len=*), intent(in)  :: str1,str2
    integer :: i,ia1,ia2,nstr1,nstr2
    integer, parameter :: ua=iachar('A'), uz=iachar('Z')
    integer, parameter :: delta=iachar('a')-iachar('A')
    nstr1 = len_trim(str1)
    nstr2 = len_trim(str2)
    is_equal = .true.
    if(nstr1 /= nstr2) then
       is_equal = .false.
    else
       do i=1,nstr1
          ia1 = iachar(str1(i:i))
          ia2 = iachar(str2(i:i))
          if(ia1 >= ua .and. ia1 <= uz) ia1 = ia1 + delta ! -> lowercase
          if(ia2 >= ua .and. ia2 <= uz) ia2 = ia2 + delta ! -> lowercase
          if(ia1 /= ia2) then
             is_equal = .false.
             exit
          end if
       end do
    end if
  end function is_equal

  logical function is_blank(char)
    ! Check if a character is space or tab
    implicit none
    character, intent(in) :: char
    is_blank = .false.
    if(ichar(char) == 9 .or. ichar(char) == 32) is_blank = .true.
  end function is_blank

  integer function field_count(record) result(num)
    ! Returns the number of fields, separated by one ore more spaces in a record
    implicit none
    character(*), intent(in)  :: record  ! Input record
    integer :: i, reclen
    logical :: lastsep
    reclen = len_trim(record)
    num = 0
    if(reclen < 1) return
    lastsep = .false.
    do i=1,reclen
       if(record(i:i) /= ' ') then
          if(.not.lastsep) then
             num = num + 1
          end if
          lastsep = .true.
       else
          lastsep = .false.
       end if
    end do
  end function field_count

  integer function get_start_of_field(record, nf) result(ind)
    ! Returns the number of fields, separated by one ore more spaces in a record
    ! Returns 0 if not found
    implicit none
    character(*), intent(in)  :: record  ! Input record
    integer, intent(in)       :: nf      ! Field number
    integer :: i, num, reclen
    logical :: lastsep
    reclen = len_trim(record)
    num = 0
    ind = 0
    if(reclen < 1) return
    lastsep = .false.
    do i=1,reclen
       if(record(i:i) /= ' ') then
          if(.not.lastsep) then
             num = num + 1
             if(num == nf) then
                ind = i
                return
             end if
          end if
          lastsep = .true.
       else
          lastsep = .false.
       end if
    end do
  end function get_start_of_field

end module m_strings
