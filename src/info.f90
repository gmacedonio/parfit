subroutine info(message)
  !
  ! Write a message on the screen and in the output file
  !
  use parmod, only: nout,nterm
  implicit none
  character(len=*) :: message
  write(nterm,'(a)') trim(message)
  if(nout /= nterm) then
     write(nout,'(a)')  trim(message)
  end if
  return
end subroutine info
