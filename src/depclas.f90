subroutine depclas(npts,xg,out)
  ! Compute normalized ash loading in a list of ground points
  !
  !  Real ash loading (kg/m2) is obtained by multiplying variable out times
  !     the mass in the column for the corresponding settling velocity class
  !
  !     npts   - Number of ground points (input)
  !     xg     - Vectors containing the positions (x,y) of the ground points
  !              respect to the crater. x(i)=xg(1,i), y(i)=xg(2,i).  (input)
  !     out    - Matrix containing the ground loads (kg/m2) in the
  !              ground points, for each particle type (output)
  !
  use parmod
  implicit none
  !
  integer :: npts
  real(8) :: out(npts,ntypes),xg(2,npts)
  integer :: i,ipts,jclass
  real(8) :: coexp,ftot
  !
  do jclass=1,ntypes
     do ipts=1,npts
        out(ipts,jclass)=0.
        do i=1,nsrc
           ftot=srcmas(i)*b(i,jclass)/(pi*cdiff)
           coexp=b(i,jclass)/cdiff*((xg(1,ipts)-rex(i,jclass))**2    &
                +(xg(2,ipts)-rey(i,jclass))**2)
           !     Prevents numerical overflow/underflow
           if(abs(coexp) < 9.0) out(ipts,jclass)=out(ipts,jclass) +  &
                ftot*exp(-coexp)
        enddo
     enddo
  enddo
  !
  return
end subroutine depclas
