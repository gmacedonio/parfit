module simplelist
  ! Handle simple lists
  !
  ! Author: G. Macedonio
  integer, parameter :: ENTRY_LENGTH=8
  type :: slist
     integer :: maxentry=0        ! Max number of entries
     integer :: n=0               ! Number of entries in the list
     character(len=ENTRY_LENGTH), allocatable :: str(:)
  end type slist

contains

  subroutine list_create(newlist,maxentry)
    ! Allocate memory for a list
    type(slist), intent(inout) :: newlist
    integer, intent(in) :: maxentry
    allocate(newlist%str(maxentry))
    newlist%maxentry = maxentry
    newlist%n = 0
  end subroutine list_create

  subroutine list_add_entry(list,string,ierr)
    ! Add an entry in the list
    ! return 0=OK, 1=list not allocated, 2=Too many entries, 3=Duplicate entry, 
    type(slist), intent(inout) :: list    
    character(len=ENTRY_LENGTH), intent(in) :: string
    integer, intent(out) :: ierr
    ierr=0
    if(list%maxentry == 0 .or. .not. allocated(list%str)) then
       ierr = 1
       return
    end if
    do i=1,list%n
       if(list%str(i)==string) then
          ierr = 3
          return
       end if
    end do
    if(list%n >= list%maxentry) then
       ierr = 2
       return
    end if
    list%n = list%n + 1
    list%str(list%n) = string
  end subroutine list_add_entry
  
end module simplelist
