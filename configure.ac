#
# This is the autoconf configuration file for parfit
#
# Process this file with autoconf to produce a configure script.
#
# To produce files: configure, Makefiles and Scripts
#    $ automake
#    $ autoconf
#
# Eventually, if you receive errors, you need to regenerate
# file aclocal.m4 with the command:
#    $ autoreconf
#  or
#    $ aclocal
#    $ autoconf
#
#
# Force an error if autoconf version is earlier than 2.63 (leave commented)
# AC_PREREQ([2.63])
#
# Autoconf initialization [Package name], [version], [email for problems]
AC_INIT([parfit], [3.0.0-beta.6], [giovanni.macedonio@ingv.it])
#
# Get git version
AS_IF([test -d .git ], [git describe --always > .gitversion])
AC_SUBST([GIT_VERSION], [`cat .gitversion`])
#
AC_CONFIG_AUX_DIR(autoconf)
# dnl Set the macro dir equal to the aux dir
#AC_CONFIG_MACRO_DIR([m4])
#
# Automake initialization (designed for GNU)
AM_INIT_AUTOMAKE([gnu -Wall -Werror])
#
# Set default prefix (where directory bin is created)
# This is the default top directory of the installation
# AC_PREFIX_DEFAULT(`pwd`)
AC_PREFIX_DEFAULT($HOME)
#
# Set the build variables: build_cpu, build_vendor and build_os
# It needs files config.guess and config.sub
AC_CANONICAL_BUILD
#
# Set the canonical host system: host_os, host_cpu and host_vendor
AC_CANONICAL_HOST
#
# Allows modification of the program file names (keep commented)
#AC_CANONICAL_TARGET
#AC_ARG_PROGRAM
#
# Set language for configuration tests
AC_LANG(Fortran)
#
# Set the AS_PROG_MKDIR_P (usually mkdir -p)
AC_PROG_MKDIR_P
#
# Search for Fortran compilers (used if environment variable FC is not set)
AC_PROG_FC
#
# Guess default Fortran linker flags
AC_FC_LIBRARY_LDFLAGS
#
# Check for existence of makedepf90
AC_CHECK_PROG([MAKEDEPF90], [makedepf90], [makedepf90])
#
# Generate instructions for ranlib
AC_PROG_RANLIB
#
AC_SUBST([DO_NOT_MODIFY_MESSAGE],
  ["DO NOT MODIFY (This file was generated automatically)"])
#
# List of Makefiles to be processed
AC_CONFIG_FILES([Makefile
		src/config.f90
		src/Makefile
])

#
# Produce all the output
AC_OUTPUT
#
# Write configuration on the screen
AC_MSG_NOTICE([---------------------------------------------------------])
AC_MSG_NOTICE([Configuration complete - $PACKAGE_NAME-$PACKAGE_VERSION])
AC_MSG_NOTICE([Git version: $GIT_VERSION])
AC_MSG_NOTICE([])
AC_MSG_NOTICE([Fortran 90 compiler:        FC=$FC])
AC_MSG_NOTICE([Fortran flags:              FCFLAGS=$FCFLAGS])
AC_MSG_NOTICE([Linker flags:               LDFLAGS=$LDFLAGS])
#
AC_MSG_NOTICE([Install prefix:             --prefix=$prefix])
AC_MSG_NOTICE([Executables install prefix: --exec_prefix=$exec_prefix])
AC_MSG_NOTICE([Binary directory:           --bindir=$bindir])
AC_MSG_NOTICE([---------------------------------------------------------])
