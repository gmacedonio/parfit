function igamma(a,x)
  !
  ! Computes the lower incomplete gamma function
  !
  ! \begin{equation}
  !   \gamma (a,x) = \int_0^x  x^{a-1} e^{-x} dx =
  !        x^a e^{-x} \sum_{n=0}^\infty \frac{x^n}{a (a+1) \dots (a+n)}
  ! \end{equation}
  !
  implicit none
  !
  ! Return value
  real(8) :: igamma
  !
  ! Arguments
  real(8), intent(in) :: a,x
  !
  ! Local variables
  real(8) :: eps = 1e-7     ! Default tolerance for SINGLE precision
  real(8) :: term,somma
  integer :: i,maxit
  !
  eps = 1d-9  ! Tolerance for DOUBLE precision
  !
  maxit = 1000     ! Maximum number of iterations
  !
  ! Initialize
  term  = 1d0
  somma = term
  !
  ! Series expansion
  !
  do i=1,maxit
     term = term*x/(a+i)
     somma = somma+term
     if(abs(term) < eps) exit
  enddo
  !
  igamma = x**a*exp(-x)*somma/a
  !
end function igamma
