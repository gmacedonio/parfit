module m_inpout
  ! Input/output routines
  ! This version needs F2003 or later
  !
  ! Author: Giovanni Macedonio
  !
  ! Version: 1.4 (F2003)
  !
  ! Date of first version: 26-FEB-2017
  ! Date of this  version:  2-MAR-2023
  !
  ! This module provides:
  ! subroutine generate_filename(filename,prefix,suffix)
  ! subroutine get_free_unit(unit)
  ! subroutine read_record(unit,string,status)
  ! subroutine flush_comment(string,sep)
  ! subroutine reclist_load by_unit(rlist,unit,nrec,reclen)
  ! subroutine reclist_load_by_name(rlist,finp,status,nrec,reclen)
  ! subroutine reclist_append(rlist,record)
  ! subroutine reclist_delete_record(curr)
  ! subroutine reclist_remove_comments(rlist,sep)
  ! subroutine reclist_free(rlist)
  ! subroutine reclist_print(rlist)
  ! function reclist_nrec(rlist)
  ! function reclist_get_recnum(list,recnum)
  ! function reclist_get_record(curr)
  ! function record_nfields(record)
  !
  ! RECLIST contains the following objects:
  ! procedure :: append           ! Append a new record at end of list
  ! procedure :: remove_comments  ! Remove comments
  ! procedure :: nrec             ! Returns the number of records
  ! procedure :: free             ! Free the list
  ! procedure :: print            ! Print the list
  ! generic   :: load             ! Load by file unit or by file name
  ! generic   :: get              ! Get current record or by number
  !
  !
  implicit none
  integer, parameter, private :: DEFAULT_MAX_RECLEN=255
  integer, parameter, private :: MAX_FILE_UNIT_NUMBER=1000

  ! Define a list of records
  type :: RECLIST
     character(len=:), allocatable :: rec
     type(RECLIST),    pointer :: next => NULL()
   contains
     procedure :: append => reclist_append  ! Append a new record at end of list
     procedure :: remove_comments => reclist_remove_comments
     procedure :: nrec   => reclist_nrec    ! Returns the number of records
     procedure :: free   => reclist_free    ! Free the list
     procedure :: print  => reclist_print   ! Print the list
     generic   :: load   => load_by_unit,load_by_name
     procedure :: load_by_unit => reclist_load_by_unit
     procedure :: load_by_name => reclist_load_by_name
     generic   :: get    => get_record, get_recnum
     procedure :: get_record => reclist_get_record
     procedure :: get_recnum => reclist_get_recnum
  end type RECLIST

contains

  subroutine generate_filename(filename,prefix,suffix)
    ! Generate a file name
    ! filename = prefix.suffix
    implicit none
    character(len=*), intent(out) :: filename
    character(len=*), intent(in) :: prefix,suffix
    filename = trim(prefix)//trim(suffix)
  end subroutine generate_filename

  subroutine get_free_unit(unit)
    ! Returns the first free file unit number
    implicit none
    integer, intent(out) :: unit
    logical :: lod
    integer :: i
    unit = -1
    do i = 2, MAX_FILE_UNIT_NUMBER               ! Start from unit=2
       if(i == 5 .or. i == 6 .or. i == 7) cycle  ! Skip 5,6,7
       inquire(i,opened=lod)
       if(.not.lod) then
          unit=i
          return
       end if
    end do
  end subroutine get_free_unit

  subroutine read_record(unit,string,status)
    ! Read a string form file
    ! Return status: OK=0, END=1, ERR=-1
    implicit none
    integer, intent(in) :: unit
    integer, intent(out) :: status
    character(len=*), intent(out) :: string
    status=0
    read(unit,'(a)',end=10,err=20) string
    return
10  status=1  ; return  ! END
20  status=-1 ; return  ! ERROR
  end subroutine read_record

  subroutine flush_comment(string,sep)
    ! Clean a string right to the characters in the string sep
    implicit none
    character(len=*), intent(inout) :: string
    character(len=*), intent(in) :: sep   ! Set of start-of-comment characters
    integer :: i,j,lsep
    logical :: found
    found=.false.
    lsep = len_trim(sep)
    do i=1,len(string)
       if(.not.found) then
          do j=1,lsep
             if(string(i:i) == sep(j:j)) then
                found = .true.
                exit
             end if
          end do
       end if
       if(found) string(i:i) = ' '
    end do
  end subroutine flush_comment

  subroutine reclist_load_by_unit(rlist,unit,nrec,reclen)
    ! Load a file and store records in a record list
    implicit none
    class(RECLIST), target :: rlist
    integer, intent(in) :: unit  ! Input file unit
    integer, optional, intent(out) :: nrec   ! Number of read records
    integer, optional, intent(in) :: reclen  ! Optional, max record length
    character(len=:), allocatable :: record  ! Work space (record)
    class(RECLIST), pointer :: last
    integer :: irec,rlen
    rlen = DEFAULT_MAX_RECLEN
    if(present(reclen)) rlen = reclen
    allocate(character(len=rlen) :: record)
    last => rlist
    irec = 0
    do
       read(unit,'(a)',end=20) record

       ! Append (TODO)
       ! call last%append(record)

       if(allocated(last%rec)) then
          allocate(last%next)
          last => last%next
       end if
       if(.not.allocated(last%rec)) &
            allocate(character(len=len_trim(record)) :: last%rec)
       last%rec = trim(record)   ! Copy
       last%next => NULL()

       irec = irec + 1
    end do
20  continue
    if(present(nrec)) nrec = irec
    deallocate(record)
  end subroutine reclist_load_by_unit

  subroutine reclist_load_by_name(rlist,finp,status,nrec,reclen)
    ! Load a file and store records in a record list
    implicit none
    class(RECLIST), target :: rlist
    character(len=*), intent(in) :: finp     ! Input file name
    integer, intent(out) :: status
    integer, optional, intent(out) :: nrec   ! Number of read records
    integer, optional, intent(in) :: reclen  ! Optional, max record length
    integer :: ninp,rrec
    status = 0
    call get_free_unit(ninp)
    open(ninp,file=finp,status='old',err=30)
    if(present(reclen)) then
       call reclist_load_by_unit(rlist,ninp,nrec=rrec,reclen=reclen)
    else
       call reclist_load_by_unit(rlist,ninp,nrec=rrec)
    end if
    if(present(nrec)) nrec = rrec
    close(ninp)
    return
30  status = 1  ! Return with error
  end subroutine reclist_load_by_name

  subroutine reclist_append(rlist,record)
    ! Append a record to the end of the list
    ! This routine is not efficient since needs to scan the list
    ! to find last entry
    class(RECLIST), target :: rlist
    character(len=*), intent(in) :: record
    class(RECLIST), pointer :: curr
    curr => rlist
    do while(associated(curr%next))
       curr => curr%next  ! Scan the list
    end do
    ! Reached end of list
    if(allocated(curr%rec)) then
       allocate(curr%next)
       curr => curr%next
    end if
    allocate(character(len=len_trim(record)) :: curr%rec)
    curr%rec = trim(record)   ! Copy
    curr%next => NULL()
  end subroutine reclist_append

  subroutine reclist_delete_record(curr)
    ! Delete the current element from the list
    class(RECLIST), pointer :: curr,next
    if(associated(curr%next)) then
       next => curr%next
       if(allocated(next%rec)) then
          curr%rec = next%rec
          deallocate(next%rec)
          curr%next => next%next
          deallocate(next)
       end if
    else
       deallocate(curr%rec)
    end if
  end subroutine reclist_delete_record

  subroutine reclist_remove_comments(rlist,sep)
    ! Remove comments, leading and trailing spaces and delete empty records
    class(RECLIST), target  :: rlist
    class(RECLIST), pointer :: curr
!!$ class(RECLIST), pointer :: next
    character(len=*), intent(in) :: sep   ! Set of start-of-comment characters
    !character(len=:), pointer :: newrec
    integer :: rlen
    curr => rlist
    do while(associated(curr))
       if(.not.allocated(curr%rec)) exit
       call flush_comment(curr%rec,sep)
       rlen = len_trim(adjustl(curr%rec))
       if(rlen==0) then
          !
!!$          if(associated(curr%next)) then
!!$             next => curr%next
!!$             if(allocated(curr%next%rec)) then
!!$                curr%rec = next%rec ! Copy
!!$                deallocate(next%rec)
!!$                curr%next => curr%next%next
!!$                deallocate(next)
!!$             end if
!!$          else
!!$             deallocate(curr%rec)
!!$          end if
          !
          call reclist_delete_record(curr)
          cycle
       end if
       curr => curr%next
    end do

  end subroutine reclist_remove_comments

  subroutine reclist_free(rlist)
    ! Free a reclist
    class(RECLIST), target  :: rlist
    class(RECLIST), pointer :: curr
    curr => rlist
    do while(associated(curr))
       if (allocated(curr%rec)) deallocate(curr%rec)
       curr => curr%next  ! Scan the list
    end do
  end subroutine reclist_free

  subroutine reclist_print(rlist)
    ! Print the content of the list
    class(RECLIST), target :: rlist
    class(RECLIST), pointer :: curr
    curr => rlist
    do while(associated(curr))
       if(.not.allocated(curr%rec)) exit
       write(*,'(a)') trim(curr%rec)
       curr => curr%next  ! Scan the list
    end do
  end subroutine reclist_print

  function reclist_nrec(rlist) result(nrec)
    ! Returns the number of elements in the list
    class(RECLIST), intent(in), target :: rlist
    class(RECLIST), pointer :: curr
    integer :: nrec
    nrec = 0
    curr => rlist
    do while(associated(curr))
       if(.not.allocated(curr%rec)) exit
       nrec = nrec + 1
       curr => curr%next
    end do
  end function reclist_nrec

  function reclist_get_recnum(list,recnum) result(record)
    ! Returns a pointer to record number recnum
    class(RECLIST), intent(in), target :: list
    integer, intent(in) :: recnum
    character(len=:), pointer :: record
    class(RECLIST), pointer :: curr
    integer :: nrec
    curr => list
    nrec = 0
    do while(associated(curr))
       if (allocated(curr%rec)) nrec = nrec + 1
       if(nrec == recnum) then
          record => curr%rec
          return
       end if
       curr => curr%next  ! Scan the list
    end do
    record => NULL()  ! recnum > list%count()
  end function reclist_get_recnum

  function reclist_get_record(list,curr) result(record)
    ! Returns the current record in the list and update the pointer
    ! A pointer to NULL is returned if end-of-list
    class(RECLIST), intent(in), target :: list
    class(RECLIST), pointer   :: curr
    character(len=:), pointer :: record
    if(.not.associated(curr)) curr=> list
    if (allocated(curr%rec)) then
       record => curr%rec
    else
       record => NULL()
    end if
    curr => curr%next
  end function reclist_get_record

  function record_nfields(record,sep) result(nf)
    ! Returns the number of fields in a record
    ! Fields are separated by one or more spaces or by the optional
    ! separation character SEP
    implicit none
    character(len=*), intent(in) :: record
    character, optional, intent(in) :: sep
    integer :: nf
    integer :: i,rlen
    rlen = len(record)
    nf = 0
    if(rlen == 0) return
    if(.not.present(sep)) then
       ! Counts the space-character transitions (eliminates multiple spaces)
       if(.not.isspace(record(1:1))) nf=nf+1
       do i=1,rlen-1
          if(isspace(record(i:i)).and..not.isspace(record(i+1:i+1))) nf = nf+1
       end do
    else
       ! NF = number of separation characters + 1
       nf = 1
       do i=1,rlen
          if(record(i:i) == sep) nf = nf+1
       end do
    end if
  contains
    logical function isspace(c)
      ! Check whether c is a space
      character, intent(in) :: c
      isspace = (c == ' ')
    end function isspace
  end function record_nfields

end module m_inpout
