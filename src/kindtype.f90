!***************************************************************
!*
!*    Module for kind definition
!*
!***************************************************************
module kindtype
  implicit none
  !
  ! Single precision: set RP=4
  ! Double precision: set RP=8
  integer, parameter :: rp = 8
  !
  ! Integer precision
  integer, parameter :: ip=4
  !
end module kindtype
