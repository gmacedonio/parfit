module m_angles
  ! Angles conversions
  !
  ! Author: Giovanni Macedonio
  !
  ! Version: 1.0
  !
  ! Date of first version:  6-MAY-2018
  ! Date of this  version:  6-MAY-2018
  !
  ! Provides:
  ! function rad(deg)
  ! function deg(rad)
  ! function deg_from_gon(gon)
  ! function gon_from_deg(deg)
  ! subroutine dec2esa(decimal,degrees,minutes,seconds)
  ! subroutine esa2dec(decimal,degrees,minutes,seconds)
  !
  implicit none
  real(8), parameter, private :: deg2rad_r8=0.017453292519943296d0
  real(4), parameter, private :: deg2rad_r4=0.017453292
  real(8), parameter, private :: rad2deg_r8=57.29577951308232d0
  real(4), parameter, private :: rad2deg_r4=57.2957795
  real(8), parameter, private :: gon2rad_r8=0.015707963267948966d0
  real(4), parameter, private :: gon2rad_r4=0.015707963
  real(8), parameter, private :: rad2gon_r8=63.66197723675813d0
  real(4), parameter, private :: rad2gon_r4=63.6619772


  interface rad
     module procedure rad_r4
     module procedure rad_r8
  end interface rad

  interface deg
     module procedure deg_r4
     module procedure deg_r8
  end interface deg

  interface deg_from_gon
     module procedure deg_from_gon_r4
     module procedure deg_from_gon_r8
  end interface deg_from_gon

  interface gon_from_deg
     module procedure gon_from_deg_r4
     module procedure gon_from_deg_r8
  end interface gon_from_deg

  interface rad_from_gon
     module procedure rad_from_gon_r4
     module procedure rad_from_gon_r8
  end interface rad_from_gon

  interface gon_from_rad
     module procedure gon_from_rad_r4
     module procedure gon_from_rad_r8
  end interface gon_from_rad

  interface dec2esa
     module procedure dec2esa_r4
     module procedure dec2esa_r8
  end interface dec2esa

  interface esa2dec
     module procedure esa2dec_r4
     module procedure esa2dec_r8
  end interface esa2dec

contains

  real(4) function rad_r4(degrees) result(radians)
    ! Convert degrees to radians
    real(4), parameter :: deg2rad=deg2rad_r4
    real(4), intent(in) :: degrees
    radians = degrees*deg2rad
  end function rad_r4

  real(8) function rad_r8(degrees) result(radians)
    ! Convert degrees to radians
    real(8), parameter :: deg2rad=deg2rad_r8
    real(8), intent(in) :: degrees
    radians = degrees*deg2rad
  end function rad_r8

  real(4) function deg_r4(radians) result(degrees)
    ! Convert radians to degrees
    real(4), parameter :: rad2deg=rad2deg_r4
    real(4), intent(in) :: radians
    degrees = radians*rad2deg
  end function deg_r4

  real(8) function deg_r8(radians) result(degrees)
    ! Convert radians to degrees
    real(8), parameter :: rad2deg=rad2deg_r8
    real(8), intent(in) :: radians
    degrees = radians*rad2deg
  end function deg_r8

  real(4) function deg_from_gon_r4(gon) result(degrees)
    ! Convert gradians (gon) to degrees
    real(4), parameter :: gon2deg=10./9.
    real(4), intent(in) :: gon
    degrees = gon2deg*gon
  end function deg_from_gon_r4

  real(8) function deg_from_gon_r8(gon) result(degrees)
    ! Convert gradians (gon) to degrees
    real(8), parameter :: gon2deg=10d0/9d0
    real(8), intent(in) :: gon
    degrees = gon2deg*gon
  end function deg_from_gon_r8

  real(4) function gon_from_deg_r4(degrees) result(gon)
    ! Convert degrees to gradians (gon)
    real(4), parameter :: deg2gon=9./10.
    real(4), intent(in) :: degrees
    gon = deg2gon*degrees
  end function gon_from_deg_r4

  real(8) function gon_from_deg_r8(degrees) result(gon)
    ! Convert degrees to gradians (gon)
    real(8), parameter :: deg2gon=9d0/10d0
    real(8), intent(in) :: degrees
    gon = deg2gon*degrees
  end function gon_from_deg_r8

  real(4) function rad_from_gon_r4(gon) result(rad)
    ! Convert gradians (gon) to radians
    real(4), parameter :: gon2rad=gon2rad_r4
    real(4), intent(in) :: gon
    rad = gon2rad*gon
  end function rad_from_gon_r4

  real(8) function rad_from_gon_r8(gon) result(rad)
    ! Convert gradians (gon) to radians
    real(8), parameter :: gon2rad=gon2rad_r8
    real(8), intent(in) :: gon
    rad = gon2rad*gon
  end function rad_from_gon_r8

  real(4) function gon_from_rad_r4(rad) result(gon)
    ! Convert radians to gradians (gon)
    real(4), parameter :: rad2gon=rad2gon_r4
    real(4), intent(in) :: rad
    gon = rad2gon*rad
  end function gon_from_rad_r4

  real(8) function gon_from_rad_r8(rad) result(gon)
    ! Convert radians to gradians (gon)
    real(8), parameter :: rad2gon=rad2gon_r8
    real(8), intent(in) :: rad
    gon = rad2gon*rad
  end function gon_from_rad_r8

  subroutine dec2esa_r4(decimal,degrees,minutes,seconds)
    ! Convert decimal degrees to degree, minute, second
    implicit none
    real(4), intent(in)  :: decimal
    integer, intent(out) :: degrees,minutes
    real(4), intent(out) :: seconds
    real(4) :: tmp
    degrees = int(decimal)
    tmp = 60.0*(decimal-real(degrees))
    minutes = int(tmp)
    seconds = (tmp-real(minutes))*60.0
  end subroutine dec2esa_r4

  subroutine dec2esa_r8(decimal,degrees,minutes,seconds)
    ! Convert decimal degrees to degree, minute, second
    implicit none
    real(8), intent(in)  :: decimal
    integer, intent(out) :: degrees,minutes
    real(8), intent(out) :: seconds
    real(8) :: tmp
    degrees = int(decimal)
    tmp = 60d0*(decimal-real(degrees))
    minutes = int(tmp)
    seconds = (tmp-real(minutes))*60d0
  end subroutine dec2esa_r8

  subroutine esa2dec_r4(decimal,degrees,minutes,seconds)
    ! Convert degree, minute, second to decimal degrees
    implicit none
    real(4), intent(out) :: decimal
    integer, intent(in)  :: degrees,minutes
    real(4), intent(in)  :: seconds
    decimal = real(degrees)+real(minutes)/60.0+seconds/3600.0
  end subroutine esa2dec_r4

  subroutine esa2dec_r8(decimal,degrees,minutes,seconds)
    ! Convert degree, minute, second to decimal degrees
    implicit none
    real(8), intent(out) :: decimal
    integer, intent(in)  :: degrees,minutes
    real(8), intent(in)  :: seconds
    decimal = real(degrees)+real(minutes)/60d0+seconds/3600d0
  end subroutine esa2dec_r8

end module m_angles
