module parmod
  ! Fortran module for parfit
  implicit none
  real(8), parameter :: pi=3.14159265358979d0    ! Greek PI
  integer :: ntypes                     ! Number of particles types
  integer :: nlev                       ! Number of wind levels
  integer :: nzlev                      ! Number of z-layers
  integer :: nsrc                       ! Number of source points
  real(8) :: cdiff                      ! Diffusion coefficient
  real(8), pointer :: zeta(:)             ! Z of the layers
  real(8), pointer :: windx(:), windy(:)  ! Wind profile
  integer :: wdate(3)                     ! Wind date (day,month,year)
  real(8), allocatable :: dz(:)               ! DZ of the layers
  real(8), allocatable :: zsrc(:)             ! Source points elevations
  real(8), allocatable :: rex(:,:),rey(:,:)   ! Positions of the gauss-centers
  real(8), allocatable :: b(:,:)              ! Gauss function prefix
  real(8), allocatable :: vvj(:,:)            ! Settling velocities
  real(8), allocatable :: spec(:,:)           ! Spectra in the deposit
  real(8), allocatable :: srcmas(:)           ! Mass in the sources points
  !
  real(8), allocatable :: dxdist(:),dydist(:) ! Work vectors for parcumula
  real(8), allocatable :: dvinv(:)            ! Work vectors for parcumula
  !.... Unit numbers (in common)
  integer :: nout = 6                         ! Output file unit (default)
  integer, parameter :: nterm= 6              ! Output terminal (default=6)

contains

  subroutine write_out(message)
    ! Write a message on the screen and in the output file
    implicit none
    character(len=*) :: message
    write(nterm,'(a)') trim(message)
    if(nout /= nterm) then
       write(nout,'(a)')  trim(message)
    end if
    return
  end subroutine write_out

end module parmod
