program typsec2gs
  ! Trasforma i files delle sezioni es (SITE_juveniles.dat, SITE_lithics.dat,
  ! SITE_crystals.dat nel file ground_spectra.inp
  !
  ! Author: G.Macedonio
  !
  ! Version: 1.0
  !
  ! Date of first version: 10-AUG-2017
  ! Date of this  version: 10-AUG-2017
  !
  use simplelist
  implicit none
  integer, parameter :: ninp=1,nout=2
  character(len=*), parameter :: fout='typsec2gs.out'  ! Output file name
  character(len=80), allocatable :: file(:)    ! Section files
  character(len=255) :: string                 ! Record
  logical :: have_nclass,have_type,have_phi,have_dens,have_shap,found
  integer :: i,j,k,l,ierr
  real :: secwt      ! Total weight in the section (temporary variable)
  real :: wtot       ! Auxiliary variable
  ! Deposit
  integer :: ntypes  ! Number of file (arguments in the command line)
  integer :: totypes ! Total types of particles
  integer :: maxsect ! Maximum number of sections
  type(slist) :: seclist          ! List of all the sections
  ! Section
  type :: section
     character(len=ENTRY_LENGTH) :: label  ! Label of the section
     real :: load                          ! Load of particles in the section
     real, allocatable :: wt(:)            ! wt% of the particles in each class
  end type section
  ! Particle type (file)
  type :: ptype
     character(len=10) :: name            ! Name of the particle type
     integer :: nc                        ! Number of classes
     integer :: nsec                      ! Number of sections
     real, allocatable :: phi(:)          ! Phi (1:nc)
     real, allocatable :: dens(:)         ! Particle density (1:nc)
     real, allocatable :: shap(:)         ! Particle shape/sphericity (1:nc)
     type(section), allocatable :: sec(:) ! Section (1:nsec)
  end type ptype
  !
  type(ptype), allocatable :: pt(:)       ! Particle type (one per file)
  !
  ntypes = iargc()
  if(ntypes < 1) then
     write(*,'(''Usage: typsec2gs file1 file2 ...'')')
     call exit(1)
  end if

  allocate(file(ntypes))
  allocate(pt(ntypes))
  do i=1,ntypes
     call getarg(i,file(i))
  end do

  ! Open particles type files
  totypes=0
  maxsect=0
  do i=1,ntypes
     open(ninp,file=file(i),status='old',err=900)
     have_nclass = .false.
     have_type   = .false.
     have_phi    = .false.
     have_dens   = .false.
     have_shap   = .false.
     ! First pass: Count the classes
     do
        read(ninp,'(a)',end=20) string
        if(string(1:1) == '#') cycle  ! Skip comments
        if(string == '')       cycle  ! Skip empty strings
        if(string(1:5) == 'TYPE:') then
           if(have_type) then
              write(*,'(''Error: "TYPE:" already specified'')')
              call exit(1)
           end if
           read(string(6:),*,err=901,end=901) pt(i)%name
           have_type = .true.
        end if
        if(string(1:7) == 'NCLASS:') then
           if(have_nclass) then
              write(*,'(''Error: "NCLASS:" already specified'')')
              call exit(1)
           end if
           read(string(8:),*,end=902,err=902) pt(i)%nc
           totypes = totypes + pt(i)%nc
           have_nclass = .true.
        end if
     end do
20   continue
     if(.not.have_type) then
        write(*,'(''Error: "TYPE":" not provided in file '',a)') trim(file(i))
        call exit(1)
     end if
     if(.not.have_nclass) then
        write(*,'(''Error: "NCLASS:" not provided in file '',a)') trim(file(i))
        call exit(1)
     end if
     ! Second pass: read phi, density, shape and count sections
     rewind(ninp)
     allocate(pt(i)%phi(pt(i)%nc))
     allocate(pt(i)%dens(pt(i)%nc))
     allocate(pt(i)%shap(pt(i)%nc))
     pt(i)%nsec=0
     do
        read(ninp,'(a)',end=21) string
        if(string(1:1) == '#')       cycle  ! Skip comments
        if(string == '')             cycle  ! Skip empty strings
        if(string(1:5) == 'TYPE:')   cycle  ! Skip TYPE
        if(string(1:7) == 'NCLASS:') cycle  ! Skip NCLASS
        if(string(1:4) == 'PHI:') then
           if(have_phi) then
              write(*,'(''Error: "PHI:" already specified'')')
              call exit(1)
           end if
           read(string(5:),*,end=903,err=903) (pt(i)%phi(j),j=1,pt(i)%nc)
           have_phi=.true.
           cycle
        end if
        if(string(1:8) == 'DENSITY:') then
           if(have_dens) then
              write(*,'(''Error: "DENSITY:" already specified'')')
              call exit(1)
           end if
           read(string(9:),*,end=904,err=904) (pt(i)%dens(j),j=1,pt(i)%nc)
           have_dens=.true.
           cycle
        end if
        if(string(1:6) == 'SHAPE:') then
           if(have_shap) then
              write(*,'(''Error: "SHAPE:" already specified'')')
              call exit(1)
           end if
           read(string(7:),*,end=905,err=905) (pt(i)%shap(j),j=1,pt(i)%nc)
           have_shap=.true.
           cycle
        end if
        pt(i)%nsec = pt(i)%nsec + 1
     end do
21   continue
     if(.not.have_phi) then
        write(*,'(''Error: "PHI:" not provided in file '',a)') trim(file(i))
        call exit(1)
     end if
     if(.not.have_dens) then
        write(*,'(''Error: "DENSITY:" not provided in file '',a)') trim(file(i))
        call exit(1)
     end if
     if(.not.have_shap) then
        write(*,'(''Error: "SHAPE:" not provided in file '',a)') trim(file(i))
        call exit(1)
     end if
     ! Third pass: read sections
     rewind(ninp)
     allocate(pt(i)%sec(pt(i)%nsec))
     do j=1,pt(i)%nsec
        allocate(pt(i)%sec(j)%wt(pt(i)%nc))
        pt(i)%sec(j)%wt = 0.0  ! Clear
     end do
     k=0
     do
        read(ninp,'(a)',end=22) string
        if(string(1:1) == '#')        cycle ! Skip comments
        if(string == '')              cycle ! Skip empty strings
        if(string(1:5) == 'TYPE:')    cycle ! Skip TYPE
        if(string(1:7) == 'NCLASS:')  cycle ! Skip NCLASS
        if(string(1:4) == 'PHI:')     cycle ! Skip PHI
        if(string(1:8) == 'DENSITY:') cycle ! Skip DENSITY
        if(string(1:6) == 'SHAPE:')   cycle ! Skip SHAPE
        ! This is a section
        k=k+1
        if(k > pt(i)%nsec) cycle
        read(string,*,end=906,err=906) pt(i)%sec(k)%label,pt(i)%sec(k)%load, &
             (pt(i)%sec(k)%wt(j),j=1,pt(i)%nc)
     end do
22   continue
     if(k /= pt(i)%nsec) then
        write(*,'(''Error: something wrong in file '',a)') trim(file(i))
        call exit(1)
     end if
     maxsect = maxsect + pt(i)%nsec
     close(ninp)
  end do

  ! Create the list of sections
  call list_create(seclist,maxsect)
  do i=1,ntypes
     do j=1,pt(i)%nsec
        call list_add_entry(seclist,pt(i)%sec(j)%label,ierr)
     end do
  end do
  !
  ! Write output
  open(nout,file=fout,status='unknown',err=910)
  write(nout,'(i4,25x,''# NTYPES'')') totypes
  do i=1,ntypes
     do j=1,pt(i)%nc
        write(nout,'(e14.7,1x,f6.1,1x,f5.3,2x,''#'',1x,i3,1x,''Diameter(m), &
             &density, shape ('',a,'')'')') 1d-3*2d0**(-pt(i)%phi(j)), &
             pt(i)%dens(j),pt(i)%shap(j),(i-1)*pt(i)%nc+j,trim(pt(i)%name)
     end do
  end do
  write(nout,'(i4,25x,''# NSECT (Number of sections)'')') seclist%n
  do j=1,seclist%n
     write(nout,'(a8,12x,''# LABEL Section '',i3)') seclist%str(j),j
     ! Calcola il carico totale della sezione (somma dei carichi di ciascun
     ! tipo di particelle)
     secwt = 0.0
     do i=1,ntypes
        do k=1,pt(i)%nsec
           if(seclist%str(j) == pt(i)%sec(k)%label) then
              secwt = secwt + pt(i)%sec(k)%load
              exit
           end if
        end do
     end do
     do i=1,ntypes
        found = .false.
        ! Calcola la somma delle percentuali di ciascun tipo di particella
        ! in ciascuna sezione (wtot compreso tra 0 e 100)
        do k=1,pt(i)%nsec
           if(seclist%str(j) == pt(i)%sec(k)%label) then
              wtot = sum(pt(i)%sec(k)%wt(1:pt(i)%nc))
              found = .true.
              exit   ! if found==.true, then k is good
           end if
        end do
        if(found) then
           do l=1,pt(i)%nc
              write(nout,'(f9.5,21x,''#'',1x,i3,1x,a,1x,''Phi: '',f5.2)') &
                   100.*pt(i)%sec(k)%wt(l)*pt(i)%sec(k)%load/wtot/secwt, &
                   (i-1)*pt(i)%nc+l,trim(pt(i)%name),pt(i)%phi(l)
           end do
        else
           do l=1,pt(i)%nc
              write(nout,'(f9.5,21x,''#'',1x,i3,1x,a,1x,''Phi: '',f5.2)') &
                   0.0,(i-1)*pt(i)%nc+l,trim(pt(i)%name),pt(i)%phi(l)
           end do
        end if
     end do
  end do

  close(nout)

  call exit(0)
900 write(*,'(''Error: cannot open file '',a)') trim(file(i))
  call exit(1)
901 write(*,'(''Error reading TYPE in file '',a)') trim(file(i))
  call exit(1)
902 write(*,'(''Error reading NCLASS in file '',a)') trim(file(i))
  call exit(1)
903 write(*,'(''Error reading PHI in file '',a)') trim(file(i))
  call exit(1)
904 write(*,'(''Error reading DENSITY in file '',a)') trim(file(i))
  call exit(1)
905 write(*,'(''Error reading SHAPE in file '',a)') trim(file(i))
  call exit(1)
906 write(*,'(''Error reading Section in file '',a)') trim(file(i))
  call exit(1)
910 write(*,'(''Error: cannot open output file '',a)') trim(fout)
  call exit(1)
end program typsec2gs
