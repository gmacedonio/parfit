module m_windutils
  !
  ! Read/write winds in the hazmat format
  !
  ! Author: Giovanni Macedonio
  !
  ! Date of this version: 29-MAR-2023
  !
  use m_errors
  use m_inpout
  implicit none

  ! Wind field file (hazmap style)
  type :: hazwinds
     integer :: nz
     integer :: nwinds
     real(8), allocatable :: zeta(:)
     integer, allocatable :: date(:,:)  ! year, month, day
     real(8), allocatable :: windx(:,:), windy(:,:)  ! wind(zeta, time)
  end type hazwinds

contains

  subroutine hazwinds_allomem(winds, nz, nwinds)
    ! Allocate memory for the hazwinds structure
    implicit none
    class(hazwinds) :: winds
    integer, intent(in) :: nz
    integer, intent(in) :: nwinds
    integer :: status
    winds%nz = nz
    winds%nwinds = nwinds
    ! Deallocate if necessary
    if(allocated(winds%zeta)) deallocate(winds%zeta)
    if(allocated(winds%date)) deallocate(winds%date)
    if(allocated(winds%windx)) deallocate(winds%windx)
    if(allocated(winds%windy)) deallocate(winds%windy)
    ! Allocate the needed memory
    allocate(winds%zeta(0:nz),STAT=status)
    if(status /= 0) call abort_program('Cannot allocate memory in subroutine readwind')
    allocate(winds%date(3, nwinds))
    allocate(winds%windx(0:nz, nwinds),STAT=status)
    if(status /= 0) call abort_program('Cannot allocate memory in subroutine readwind')
    allocate(winds%windy(0:nz, nwinds),STAT=status)
    if(status /= 0) call abort_program('Cannot allocate memory in subroutine readwind')
  end subroutine hazwinds_allomem

  subroutine hazwinds_read(winds, fwnd)
    ! Reads the winds file and stores it in the hazwind structure
    ! Memory is allocated
    !
    ! Note: date(1)=year, date(2)=month, date(3)=day
    !
    implicit none
    class(hazwinds) :: winds
    character(len=*),intent(in) :: fwnd
    integer :: nwin, i, lev, rlev, idummy
    character(len=80) :: message
    !
    call get_free_unit(nwin)
    if(nwin < 0) call abort_program('Cannot get file unit in subroutine readwind')
    !
    open(nwin,file=fwnd,status='old',err=1001)
    read(nwin,*) winds%nz

    ! skip z-levels
    do i=1, winds%nz
       read(nwin, *, end=1002)
    end do

    ! Count wind records
    winds%nwinds = 0
    do
       read(nwin, *, end=20)  idummy, idummy, idummy, rlev  ! First record
       do lev=2, winds%nz
          read(nwin, *, end=1002) idummy, idummy, idummy, rlev
          if(lev /= rlev) then
             write(*,*) 'Inconsistent wind record at level: ', lev
             write(*,*) 'Wind profile: ', winds%nwinds+1
             write(*,*) 'Record: ', winds%nz + 1 + winds%nwinds*winds%nz + lev
             call abort_program('Abort')
          end if
       end do
       winds%nwinds = winds%nwinds + 1
    end do
20  continue

    ! Allocate memory
    call hazwinds_allomem(winds, winds%nz, winds%nwinds)

    ! Rewind file
    rewind(nwin)

    ! Read z-levels
    read(nwin, *, end=1002)   ! Skip number of z-levels
    do i=1, winds%nz
       read(nwin, *, end=1002) winds%zeta(i)
    end do
    winds%zeta(0) = 0.0

    ! Read wind profiles
    do i = 1, winds%nwinds
       do lev = 1, winds%nz
          read(nwin,*) winds%date(1, i), winds%date(2, i), winds%date(3, i), &
               rlev, winds%windx(lev, i), winds%windy(lev, i)
          if(lev /= rlev) then
             write(message,'(''Inconsistent level number in file: '',a)') trim(fwnd)
             write(*,*) 'Wind profile: ', i + 1
             write(*,*) 'Record: ', winds%nz + 1 + i*winds%nz + lev
             call abort_program(message)
          end if
       end do
       ! Set wind at ground level: extrapolate from layer n.1
       winds%windx(0, i) = winds%windx(1, i)
       winds%windy(0, i) = winds%windy(1, i)
    end do
    close(nwin)
    return
    ! Errors
1001 write(message,*) 'Cannot open file: '//trim(fwnd)
    call abort_program(message)
1002 write(message,*) 'Inconsistent number of records in file: '//trim(fwnd)
    call abort_program(message)
  end subroutine hazwinds_read

  subroutine hazwinds_write(wind, fwnd, iwnd)
    !
    ! Write a wind in a file (extract from the winds list)
    !
    ! Note: date(1)=year, date(2)=month, date(3)=day
    !
    implicit none
    class(hazwinds) :: wind
    character(len=*) :: fwnd
    integer, intent(in) :: iwnd
    integer :: nwin, lev
    character(len=80) :: message
    !
    call get_free_unit(nwin)
    if(nwin < 0) call abort_program('Cannot get file unit in subroutine hazwind_write')
    !
    open(nwin,file=fwnd,status='unknown',err=1001)
    write(nwin,'(i3)') wind%nz
    do lev=1, wind%nz
       write(nwin,'(f6.0)') wind%zeta(lev)
    enddo
    do lev = 1, wind%nz
       write(nwin,'(i4.4,3(1x,i2),2(1x,f7.3))') wind%date(1, iwnd), &
            wind%date(2, iwnd), wind%date(3, iwnd), lev, &
            wind%windx(lev, iwnd), wind%windy(lev, iwnd)
    enddo
    close(nwin)
    return
    !
    ! Errors
1001 write(message,*) 'Cannot open file: '//trim(fwnd)
    call abort_program(message)
  end subroutine hazwinds_write

  subroutine wriwind(fwnd, nzlev, zeta, windx, windy, date)
    !
    ! Write wind profile
    !
    ! Note: date(1)=day, date(2)=month, date(3)=year
    !
    implicit none
    character(len=*) :: fwnd
    integer :: nzlev
    real(8) :: zeta(0:nzlev)
    real(8) :: windx(0:nzlev),windy(0:nzlev)
    integer :: date(3)
    integer :: nwin
    integer :: i
    character(len=80) :: message
    !
    call get_free_unit(nwin)
    if(nwin < 0) call abort_program('Cannot get file unit in subroutine wriwind')
    !
    open(nwin,file=fwnd,status='unknown',err=1001)
    write(nwin,'(i3)') nzlev
    do i=1,nzlev
       write(nwin,'(f6.0)') zeta(i)
    enddo
    do i = 1,nzlev
       write(nwin,'(i4.4,3(1x,i2),2(1x,f7.3))') date(3),date(2),date(1), &
            i,windx(i),windy(i)
    enddo
    close(nwin)
    return
    !
    ! Errors
1001 write(message,*) 'Cannot open file: '//trim(fwnd)
    call abort_program(message)
  end subroutine wriwind

  subroutine hazwinds_get(winds, iwnd, windx, windy, wdate)
    ! Get wind iwnd from the winds list
    ! Assume windx, windy are properly allocated
    class(hazwinds) :: winds
    integer, intent(in) :: iwnd
    real(8), intent(out) :: windx(0:), windy(0:)
    integer, intent(out) :: wdate(3)
    if(iwnd < 1 .or. iwnd > winds%nwinds) &
         call abort_program('Invalid wind index (iwnd) in hazwinds_get')
    windx(0:winds%nz) = winds%windx(0:winds%nz, iwnd)
    windy(0:winds%nz) = winds%windy(0:winds%nz, iwnd)
    wdate(1:3) = winds%date(1:3, iwnd)
  end subroutine hazwinds_get

  subroutine genwind(nzlev,wnd,dir,wvel,windx,windy)
    !
    ! Generate a wind profile starting from a prototype wind profile
    !
    implicit none
    integer,intent(in)   :: nzlev
    real(8),intent(in)  :: wnd,dir
    real(8),intent(in)  :: wvel(0:nzlev)
    real(8),intent(out) :: windx(0:nzlev),windy(0:nzlev)
    integer :: i
    !
    do i=0,nzlev
       windx(i) = wnd*wvel(i)*cos(dir)
       windy(i) = wnd*wvel(i)*sin(dir)
    enddo
  end subroutine genwind

  subroutine genprotowind(nzlev,zeta,htropo,ntropo,wvel)
    !
    ! Generate a prototype wind (Cornell profile) normalized to one
    ! at tropopause
    !
    implicit none
    integer,intent(in)   :: nzlev,ntropo
    real(8),intent(in)  :: zeta(0:nzlev)
    real(8),intent(in)  :: htropo
    real(8),intent(out) :: wvel(0:nzlev)
    integer :: i
    !
    do i=0,ntropo
       wvel(i) = zeta(i)/htropo
    enddo
    do i=ntropo+1,nzlev               ! This loop is skipped if nzlev < ntropo+1
       wvel(i) = 0.75*wvel(ntropo)    ! 3/4 of tropopause wind
    enddo
  end subroutine genprotowind

end module m_windutils
