!> Extension of the input/output routines and data
!! Store and read data using a hash table (key = value)
module m_inpoutext
  !
  ! Author: Giovanni Macedonio
  !
  ! Date of first version: 28-FEB-2017
  ! Date of this  version: 28-MAR-2023
  !
  ! This module provides:
  ! subroutine read_and_store(finp,status,[maxreclen],[hashtable],[tablesize],[comstring])
  ! logical function check_key(key,hashtable)
  ! subroutine mandatory_input(key, val)
  ! subroutine optional(key, val, default)
  ! subroutine get_input_value(key,val[,status][,hashtable][,default])
  !
  use m_errors
  use m_inpout
  use m_strings
  use hashtbl, hash_data => hash_tbl_sll  ! hash_data is alias of hash_tbl_sll
  implicit none
  integer, parameter :: DEFAULT_MAX_KEY_LENGTH=50
  integer, parameter :: DEFAULT_MAX_VAL_LENGTH=255
  integer, parameter :: DEFAULT_MAX_COMSTRING_LENGTH=10
  !
  integer, private :: MAX_KEY_LENGTH=DEFAULT_MAX_KEY_LENGTH
  integer, private :: MAX_VAL_LENGTH=DEFAULT_MAX_VAL_LENGTH
  integer, private :: MAX_COMSTRING_LENGTH=DEFAULT_MAX_COMSTRING_LENGTH
  type(hash_data), save, target :: default_hashtable ! Uses this hash table by default
  !

  !> Mandatory input
  interface mandatory
     module procedure mandatory_int
     module procedure mandatory_real4
     module procedure mandatory_real8
     module procedure mandatory_char
     module procedure mandatory_logical
  end interface mandatory

  !> Optional input
  interface optional
     module procedure optional_int
     module procedure optional_real4
     module procedure optional_real8
     module procedure optional_char
     module procedure optional_logical
  end interface optional

  interface get_input_value
     module procedure get_input_value_int
     module procedure get_input_value_real4
     module procedure get_input_value_real8
     module procedure get_input_value_char
     module procedure get_input_value_logical
  end interface get_input_value

contains

  !> Get an integer value from the hashtable.
  !! If the key is not found aborts the program with a message
  subroutine mandatory_int(key, val)
    implicit none
    character(len=*), intent(in) :: key
    integer, intent(out) :: val
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) call abort_program('Error: '//trim(key)//' non provided')
  end subroutine mandatory_int

  !> Get a real(4) value from the hashtable.
  !! If the key is not found aborts the program with a message
  subroutine mandatory_real4(key, val)
    implicit none
    character(len=*), intent(in) :: key
    real(4), intent(out) :: val
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) call abort_program('Error: '//trim(key)//' non provided')
  end subroutine mandatory_real4

  !> Get a real(8) value from the hashtable.
  !! If the key is not found aborts the program with a message
  subroutine mandatory_real8(key, val)
    implicit none
    character(len=*), intent(in) :: key
    real(8), intent(out) :: val
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) call abort_program('Error: '//trim(key)//' non provided')
  end subroutine mandatory_real8

  !> Get a string of characters from the hashtable.
  !! If the key is not found aborts the program with a message
  subroutine mandatory_char(key, val)
    implicit none
    character(len=*), intent(in)  :: key
    character(len=*), intent(out) :: val
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) call abort_program('Error: '//trim(key)//' non provided')
  end subroutine mandatory_char

  !> Get a logical value from the hashtable.
  !! If the key is not found aborts the program with a message
  subroutine mandatory_logical(key, val)
    implicit none
    character(len=*), intent(in)  :: key
    logical, intent(out) :: val
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) call abort_program('Error: '//trim(key)//' non provided')
  end subroutine mandatory_logical

  !> Get an integer value from the hashtable.
  !! If the key is not found the default value is used
  subroutine optional_int(key, val, default)
    implicit none
    character(len=*), intent(in) :: key
    integer, intent(out) :: val
    integer, intent(in)  :: default
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) val = default
  end subroutine optional_int

  !> Get a real(4) value from the hashtable.
  !! If the key is not found the default value is used
  subroutine optional_real4(key, val, default)
    implicit none
    character(len=*), intent(in) :: key
    real(4), intent(out) :: val
    real(4), intent(in)  :: default
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) val = default
  end subroutine optional_real4

  !> Get a real(8) value from the hashtable.
  !! If the key is not found the default value is used
  subroutine optional_real8(key, val, default)
    implicit none
    character(len=*), intent(in) :: key
    real(8), intent(out) :: val
    real(8), intent(in)  :: default
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) val = default
  end subroutine optional_real8

  !> Get a string of characters from the hashtable.
  !! If the key is not found the default value is used
  subroutine optional_char(key, val, default)
    implicit none
    character(len=*), intent(in)  :: key
    character(len=*), intent(out) :: val
    character(len=*), intent(in)  :: default
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) val = default
  end subroutine optional_char

  !> Get a logical value from the hashtable.
  !! If the key is not found the default value is used
  subroutine optional_logical(key, val, default)
    implicit none
    character(len=*), intent(in)  :: key
    logical, intent(out) :: val
    logical, intent(in)  :: default
    integer :: status
    call get_input_value(key, val, status=status)
    if(status /= 0) val = default
  end subroutine optional_logical

  !> Read the input file and store values in the hash table.
  !! The hash table is created
  subroutine read_and_store(finp,status,maxreclen,hashtable,tablesize,comstring)
    ! Return: 0=OK, 1=cannot_open, 2=cannot_read
    use tokenize
    implicit none
    character(len=*), intent(in) :: finp
    integer, intent(out) :: status
    integer, optional :: maxreclen
    type(hash_data), optional, target, intent(inout) :: hashtable
    integer, optional, intent(in) :: tablesize
    character(len=MAX_COMSTRING_LENGTH), optional, intent(in) :: comstring
    type(hash_data), pointer :: curr_hashtable ! Current hashtable
    integer nrec,reclen,nentry
    character(len=:), allocatable :: record  ! Work space
    character(len=MAX_KEY_LENGTH) :: keyname
    character(len=MAX_VAL_LENGTH) :: stringval
    integer :: ninp
    integer :: varlen,vallen
    type(tokenizer) :: token
    character(len=MAX_COMSTRING_LENGTH) :: mycomstring
    integer :: istatus
    status = 0
    if(present(maxreclen)) then
       reclen = maxreclen
    else
       reclen = MAX_KEY_LENGTH + MAX_VAL_LENGTH + 10
    end if
    if(present(comstring)) then
       mycomstring = comstring
    else
       mycomstring = '#!'   ! Default start of comment
    end if
    allocate(character(len=reclen) :: record)
    !
    ! Set 'key=value' structure
    call set_tokenizer(token,token_empty,'=',token_empty)

    ! Open the input file
    call get_free_unit(ninp)
    open(ninp,file=finp,status='old',err=900)

    ! Create the hash table
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if
    if(present(tablesize)) then
       call curr_hashtable%init(tablesize)
    else
       call curr_hashtable%init()  ! Default is tbl_size (30)
    end if

    ! Scan the input file
    nrec=0
    nentry=0
    do
       call read_record(ninp,record,istatus)
       if(istatus == 1) exit  ! End of file
       if(istatus < 0) then   ! Read error
          status = 2
          return
       end if
       nrec = nrec+1
       ! TODO: The list of start-of-comment chars should be passed from extern
       call flush_comment(record,mycomstring) ! Flush comments right comstring
       ! Get the key=value pairs
       keyname = first_token(token,record,varlen)
       stringval = next_token(token,record,vallen)
       if(vallen <= 0) cycle
       nentry = nentry+1
       ! write(*,*) trim(adjustl(keyname))//' = '//trim(adjustl(stringval))
       ! Convert the key to uppercase
       call convert_to_uppercase(keyname)
       call curr_hashtable%put(trim(adjustl(keyname)),trim(adjustl(stringval)))
    end do

    ! Close input file
    close(ninp)
    ! Free memory
    deallocate(record)
    return
    !
    ! Errors
900 status = 1
  end subroutine read_and_store

  !> Check if a key is in the hashtable
  logical function check_key(key,hashtable)
    implicit none
    type(hash_data), optional, target, intent(in) :: hashtable
    character(len=*), intent(in) :: key
    type(hash_data), pointer :: curr_hashtable
    character(len=:), allocatable :: strval
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if
    call curr_hashtable%get(key,strval)
    check_key = .false.
    if(allocated(strval)) then
       deallocate(strval)
       check_key = .true.
    end if
  end function check_key

  !> Get an integer value from the hashtable
  subroutine get_input_value_int(key,val,status,hashtable,default)
    ! If status is present it is 0=OK, 1=error(key not found), 2=read_error
    ! Else issue a message and abort program
    implicit none
    type(hash_data), optional, target, intent(in) :: hashtable
    character(len=*), intent(in) :: key
    integer, intent(out) :: val
    integer, optional, intent(in)  :: default
    integer, optional, intent(out) :: status
    type(hash_data), pointer :: curr_hashtable
    character(len=:), allocatable :: strval
    if(present(status)) status=0
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if
    call curr_hashtable%get(key,strval)  ! Get value from the hashtable
    if(allocated(strval)) then
       read(strval,*,end=900,err=900) val
       deallocate(strval)
       return   ! OK: RETURN
    else
       if(present(default)) then
          val = default
          return
       end if
    end if
    if(present(status)) then
       ! Key not found
       status=1
       return
    else
       write(*,*) 'Error. Variable not found in the input file: ',trim(key)
       call abort_program
    end if
900 continue
    if(present(status)) then
       status=2
       return
    else
       write(*,*) 'Error getting input variable: ',trim(key)
       call abort_program
    end if
  end subroutine get_input_value_int

  !> Get an integer value from the hashtable
  subroutine get_input_value_real4(key,val,status,hashtable,default)
    ! If status is present it is 0=OK, 1=error(key not found), 2=read_error
    ! Else issue a message and abort program
    implicit none
    type(hash_data), optional, target, intent(in) :: hashtable
    character(len=*), intent(in) :: key
    real, intent(out) :: val
    real, optional, intent(in)  :: default
    integer, optional, intent(out) :: status
    type(hash_data), pointer :: curr_hashtable
    character(len=:), allocatable :: strval
    if(present(status)) status=0
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if
    call curr_hashtable%get(key,strval)
    if(allocated(strval)) then
       read(strval,*,end=900,err=900) val
       deallocate(strval)
       return
    else
       if(present(default)) then
          val = default
          return
       end if
    end if
    if(present(status)) then
       ! Key not found
       status=1
       return
    else
       write(*,*) 'Error. Variable not found in the input file: ',trim(key)
       call abort_program
    end if
900 continue
    if(present(status)) then
       status=2
       return
    else
       write(*,*) 'Error getting input variable: ',trim(key)
       call abort_program
    end if
  end subroutine get_input_value_real4

  !> Get an integer value from the hashtable
  subroutine get_input_value_real8(key,val,status,hashtable,default)
    ! If status is present it is 0=OK, 1=error(key not found), 2=read_error
    ! Else issue a message and abort program
    implicit none
    type(hash_data), optional, target, intent(in) :: hashtable
    character(len=*), intent(in) :: key
    real(8), intent(out) :: val
    real(8), optional, intent(in)  :: default
    integer, optional, intent(out) :: status
    type(hash_data), pointer :: curr_hashtable
    character(len=:), allocatable :: strval
    if(present(status)) status=0
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if
    call curr_hashtable%get(key,strval)
    if(allocated(strval)) then
       read(strval,*,end=900,err=900) val
       deallocate(strval)
       return
    else
       if(present(default)) then
          val = default
          return
       end if
    end if
    if(present(status)) then
       ! Key not found
       status=1
       return
    else
       write(*,*) 'Error. Variable not found in the input file: ',trim(key)
       call abort_program
    end if
900 continue
    if(present(status)) then
       status=2
       return
    else
       write(*,*) 'Error getting input variable: ',trim(key)
       call abort_program
    end if
  end subroutine get_input_value_real8

  !> Get a string of characters from the hashtable
  subroutine get_input_value_char(key,val,status,hashtable,default)
    ! If status is present it is 0=OK, 1=error(key not found), 2=read_error
    ! Else issue a message and abort program
    implicit none
    type(hash_data), optional, target, intent(in) :: hashtable
    character(len=*), intent(in) :: key
    character(len=*), intent(out) :: val
    character(len=*), optional, intent(in)  :: default
    integer, optional, intent(out) :: status
    type(hash_data), pointer :: curr_hashtable
    character(len=:), allocatable :: strval
    if(present(status)) status=0
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if

    call curr_hashtable%get(key,strval)
    if(allocated(strval)) then
       val = strval
       deallocate(strval)
       return
    else
       if(present(default)) then
          val = default
          return
       end if
    end if
    if(present(status)) then
       ! Key not found
       status=1
       return
    else
       write(*,*) 'Error. Variable not found in the input file: ',trim(key)
       call abort_program
    end if
    !
    if(present(status)) then
       status=2
       return
    else
       write(*,*) 'Error getting input variable: ',trim(key)
       call abort_program
    end if
  end subroutine get_input_value_char

  !> Get a logical value from the hashtable
  subroutine get_input_value_logical(key,val,status,hashtable,default)
    ! If status is present it is 0=OK, 1=error(key not found), 2=read_error
    ! Else issue a message and abort program
    implicit none
    type(hash_data), optional, target, intent(in) :: hashtable
    character(len=*), intent(in) :: key
    logical, intent(out) :: val
    logical, optional, intent(in)  :: default
    integer, optional, intent(out) :: status
    type(hash_data), pointer :: curr_hashtable
    character(len=:), allocatable :: strval
    if(present(status)) status=0
    if(present(hashtable)) then
       curr_hashtable => hashtable
    else
       curr_hashtable => default_hashtable
    end if
    call curr_hashtable%get(key,strval)
    if(allocated(strval)) then
       if(len_trim(strval) == 0) goto 900
       if((strval .equal. '.true.') .or. (strval .equal. 'true') .or. &
            (strval .equal. 'yes')) then
          val = .true.
       else
          val = .false.
       end if
       deallocate(strval)
       return
    else
       if(present(default)) then
          val = default
          return
       end if
    end if
    if(present(status)) then
       ! Key not found
       status=1
       return
    else
       write(*,*) 'Error. Variable not found in the input file: ',trim(key)
       call abort_program
    end if
900 continue
    if(present(status)) then
       status=2
       return
    else
       write(*,*) 'Error getting input variable: ',trim(key)
       call abort_program
    end if
  end subroutine get_input_value_logical

end module m_inpoutext
