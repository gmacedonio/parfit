subroutine split(n,x,m,in,ii,jj,s,iabv,na,maxa,ibel,nb,maxb)
  !
  !     Translated to F90 by G.Macedonio
  !
  !     This is part of algorithm 523 written
  !     by W.F. Eddy, ACM TOMS 3 (1977) 411-412
  !
  !     This subroutine takes the m points of array x whose
  !     subscripts are in array in and partitions them by the
  !     line joining the two points in array x whose subscripts
  !     are ii and jj.
  !     The subscripts of the points above the line are put into
  !     array iabv, and the subscripts of the
  !     points below are put into array ibel.
  !     na and nb are, respectively, the number of points above the
  !     line and the number below.
  !     maxa and maxb are the subscripts for array
  !     x of the point furthest above the line and the point
  !     furthest below, respectively. If either subset is null
  !     the corresponding subscript (maxa or maxb) is set to zero.
  !
  !     Formal parameters
  !
  !     Input
  !     n    integer           total number of data points
  !     x    real array (2,n)  (x,y) co-ordinates of the data
  !     m    integer           number of points in input subset
  !     in   integer array (m) subscripts for array x of the
  !     points in the input subset
  !     ii   integer           subscript for array x of one point
  !     on the partitioning line
  !     jj   integer           subscript for array x of another
  !     point on the partitioning line
  !     s    integer           switch to determine output. refer
  !     to comments below
  !
  !     Output
  !     iabv integer array (m) subscripts for array x of the
  !     points above the partitioning line
  !     na   integer           number of elements in iabv
  !     maxa integer           subscript for array x of point
  !     furthest above the line. set to zero if na is zero
  !     ibel integer array (m) subscripts for array x of the
  !     points below the partitioning line
  !     nb   integer           number of elements in ibel
  !     maxb integer           subscript for array x of point
  !     furthest below the line. set to zero if nb is zero
  !
  implicit none
  !
  integer :: n,m,ii,jj,na,maxa,nb,maxb
  real(8) :: x(2,n)
  integer ::  in(m),iabv(m),ibel(m)
  integer :: s
  !
  !     If s = 2 dont save ibel,nb,maxb.
  !     If s =-2 dont save iabv,na,maxa.
  !     Otherwise save everything
  !     If s is positive the array being partitioned is above
  !     the initial partitioning line. If it is negative, then
  !     the set of points is below.
  !
  logical :: t
  integer :: i,is
  real(8) :: dir,a,b,up,down,z,xt
  !
  t=.false.
  !
  !     Check to see if the line is vertical
  if(x(1,jj) /= x(1,ii)) goto 1
  xt=x(1,ii)
  dir=sign(1d0,x(2,jj)-x(2,ii))*sign(1,s)
  t=.true.
  goto 2
1 a=(x(2,jj)-x(2,ii))/(x(1,jj)-x(1,ii))
  b=x(2,ii)-a*x(1,ii)
2 up=0.
  na=0
  maxa=0
  down=0.
  nb=0
  maxb=0
  do i=1,m
     is=in(i)
     if(t) goto 3
     z=x(2,is)-a*x(1,is)-b
     goto 4
3    z=dir*(x(1,is)-xt)
4    if(z <= 0.) goto 5
     !
     !     The point is above the line
     if(s == (-2)) goto 6
     na=na+1
     iabv(na)=is
     if(z < up) goto 6
     up=z
     maxa=na
     goto 6
5    if(s == 2) goto 6
     if(z >= 0.) goto 6
     !
     !     The point is below the line
     nb=nb+1
     ibel(nb)=is
     if(z > down) goto 6
     down=z
     maxb=nb
6    continue
  enddo
  return
end subroutine split
