!> Module for data scaling
module m_scaling
  !
  ! Author: Giovanni Macedonio
  !
  ! Date: 22-DEC-2022
  !
  implicit none

  !> Class for direct and inverse scaling of a vector
  type :: scaling
     integer :: npar
     real(8), allocatable :: q(:)   ! Offset
     real(8), allocatable :: m(:)   ! Scale factor
   contains
     procedure :: setpar => scaling_setpar
     generic   :: scale  => scale1, scale2
     generic   :: iscale => iscale1, iscale2
     procedure :: scale1  => scaling_scale1   !< Scale to [scaled_low,scaled_up]
     procedure :: scale2  => scaling_scale2   !< Scale to [scaled_low,scaled_up]
     procedure :: iscale1 => scaling_iscale1  !< Scale to [low,up]
     procedure :: iscale2 => scaling_iscale2  !< Scale to [low,up]
  end type scaling

contains

  !> Set the scaling parameters.
  !! Components of vector up must be greater than the corresponding
  !! components of vector low
  subroutine scaling_setpar(sc, npar, low, up, scaled_low, scaled_up)
    class(scaling) :: sc
    integer, intent(in) :: npar
    real(8), intent(in) :: low(npar)
    real(8), intent(in) :: up(npar)
    real(8), intent(in) :: scaled_low
    real(8), intent(in) :: scaled_up
    if(npar <= 0) return
    if(allocated(sc%q)) deallocate(sc%q)
    if(allocated(sc%m)) deallocate(sc%m)
    allocate(sc%q(npar))
    allocate(sc%m(npar))
    sc%npar = npar
    sc%q = (scaled_up*low - scaled_low*up)/(scaled_up - scaled_low)
    sc%m = (up - low)/(scaled_up - scaled_low)
  end subroutine scaling_setpar

  !> Scale the components of a vector to the range [0:1].
  !! Overwrite the vector
  subroutine scaling_scale1(sc, x)
    class(scaling) :: sc
    real(8), intent(inout)  :: x(:)
    x = (x-sc%q)/sc%m
  end subroutine scaling_scale1

  !> Scale the components of a vector to the range [0:1]
  subroutine scaling_scale2(sc, xin, xout)
    class(scaling) :: sc
    real(8), intent(in)  :: xin(:)
    real(8), intent(out) :: xout(:)
    xout = (xin-sc%q)/sc%m
  end subroutine scaling_scale2

  !> Scale the components of a vector to the range [low:up].
  !! Overwrite the vector
  subroutine scaling_iscale1(sc, x)
    class(scaling) :: sc
    real(8), intent(inout) :: x(:)
    x = sc%q + sc%m*x
  end subroutine scaling_iscale1

  !> Scale the components of a vector to the range [low:up]
  subroutine scaling_iscale2(sc, xin, xout)
    class(scaling) :: sc
    real(8), intent(in)  :: xin(:)
    real(8), intent(out) :: xout(:)
    xout = sc%q + sc%m*xin
  end subroutine scaling_iscale2

end module m_scaling
